#ifndef BYTEAI_MATH_UTIL_H_
#define BYTEAI_MATH_UTIL_H_

#include <cmath>
#include <limits>

typedef unsigned int uint;
typedef unsigned short ushort;
typedef unsigned char byte;

const double kDoubleEpsilon = std::numeric_limits<double>::epsilon();
const double kMaxDouble = std::numeric_limits<double>::max();
const int kMaxInt = std::numeric_limits<int>::max();
const double kPI = 3.1415926536;

#include <minmax.h>

/**
 * Checks if two floats are equal.
 * NOTE: It is note the most safe way of doing it!
 */
inline bool IsEqual(double a, double b)
{
	return abs(a - b) < kDoubleEpsilon;
}

/**
 * Double a number, by using left shift.
 */
inline int DoubleInt(int a)
{
	return a << 1;
}

inline int DoubleToIntRound(double value)
{
    return int(value > 0.0 ? floor(value + 0.5) : ceil(value - 0.5)); 
}

#endif
