#ifndef BYTEAI_RECIPIENT_H_
#define BYTEAI_RECIPIENT_H_

#include "Messages.h"

enum Recipients 
{
	INVALID_RECIPIENT,
    RECIPIENT_ALL,//Send message to all recipients.
	RECIPIENT_MAP,
	RECIPIENT_NET,
    RECIPIENT_MORTAR,
    RECIPIENT_LASER,
    RECIPIENT_DROID,
    RECIPIENT_THINKGOAL,
    RECIPIENT_PLAYERMGR
};

/**
 * Base  class for system that want to receive local messages.
 */
class Recipient
{
public:
	Recipient(int id) : m_id(id) {}

    virtual ~Recipient() {}

    /**
     * Override this function to receive messages.
     */
    virtual bool HandleMessage(Message &msg) = 0;

	/**
	 * Returns the ID of the recipient.
	 */
	int GetID() {return m_id;}

	friend class MessageHandler;
private:
	uint m_id;
};

#endif