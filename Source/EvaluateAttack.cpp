#include "EvaluateAttack.h"
#include "GameManager.h"
#include "PlayerManager.h"
#include "Player.h"
#include "Messages.h"
#include "AttackGoal.h"
double EvaluateAttack::CalculateDesirability()
{
	/*
		Distance
		Own level
		enemy levels
	*/
	m_attacker = gGameMgr.GetPlayer();
	const PlayerManager *playerMgr = gGameMgr.GetPlayerMgr();
	const Player *closestEnemy = playerMgr->FindClosestEnemy(m_attacker);
	int enemyDistance = Distance(m_attacker->GetPosition(), closestEnemy->GetPosition());

	m_target = closestEnemy; //TODO: Add more logic to decide which enemy to target
	const PlayerData *targetData = m_target->GetPlayerData();
	
	const PlayerData *attackerData = m_attacker->GetPlayerData();
	//Enemy factors
	double tHealthFactor = targetData->health;
	double tLevelPrimWeaponFactor = targetData->levelPrimWeapon;
	double tLevelSecondWeaponFactor = targetData->levelSecondWeapon;

	//Our own factors
	double healthFactor = attackerData->health;
	double levelPrimFactor = attackerData->levelPrimWeapon;
	double levelSecondFactor = attackerData->levelSecondWeapon;

	//TODO: Check the following forumla. Excpected values are between 0 and 100
	double attackFactor = ((healthFactor + levelPrimFactor + levelSecondFactor)/(tHealthFactor + tLevelPrimWeaponFactor + tLevelSecondWeaponFactor + enemyDistance))*100;
	return min(attackFactor, 100);
}

AttackGoal *EvaluateAttack::GetGoal()
{
	return new AttackGoal(m_target, m_attacker);
}