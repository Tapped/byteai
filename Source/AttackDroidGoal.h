#ifndef BYTEAI_ATTACK_DROID_GOAL_H_
#define BYTEAI_ATTACK_DROID_GOAL_H_

#include <rapidjson/document.h>
#include "AI_Goals.h"
#include "Player.h"

/**
 * Atomic goal, which fires a droid.
 */
class AttackDroidGoal : public AtomicGoal
{
public:
	AttackDroidGoal(const Player *target, const Player *attacker, int range) : m_target(target), m_attacker(attacker) {}
	void Activate();

	int Process();

	void End();
private:
	static void PrecacheJSONDocument();
	static rapidjson::Document m_jsonDoc;

	const Player *m_target;
    const Player *m_attacker;
};

#endif
