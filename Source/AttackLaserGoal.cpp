#include "AttackLaserGoal.h"
#include "MessageHandler.h"
#include "Map.h"
#include "TraverseEdgeGoal.h"
#include "GameManager.h"
#include "Player.h"
 
#include <iostream>

const TilePos kTilePosPossibilities[6] =
{
	TilePos(-1, -1), TilePos(1, 1), TilePos(0, -1),
	TilePos(0, 1), TilePos(-1, 0), TilePos(1, 0)
};

rapidjson::Document AttackLaserGoal::m_jsonDoc;
 
void AttackLaserGoal::Activate()
{
    m_status = GOAL_ACTIVE;

	Clear();

	PrecacheJSONDocument();

    const char *direction = GetDirection(m_attacker->GetPosition(), m_target->GetPosition());
    if(direction == NULL)
	{
#ifdef DEBUG_LASER
		std::cout<<"Direction == NULL"<<std::endl;
#endif
		int closestNodeToTarget = INVALID_NODE_INDEX;
		int distNodeToTarget = kMaxInt;

		const Map *map = gGameMgr.GetMap();
		int fromNode = map->GetNodeIndex(m_attacker->GetPosition());

        for(int i = 0;i < 6;i++)
        {
            TilePos moveTo = m_attacker->GetPosition() + kTilePosPossibilities[i];
			int toNode = map->GetNodeIndex(moveTo);
			int distance = Distance(moveTo, m_target->GetPosition());
			if(!map->IsInaccesibleNode(toNode) && distance < distNodeToTarget)
            {
				closestNodeToTarget = toNode;
				distNodeToTarget = distance;
            }
        }

		//We are stuck if closestNodeToTarget is invalid.
		if(closestNodeToTarget != INVALID_NODE_INDEX)
		{
			AddGoal(new TraverseEdgeGoal(fromNode, closestNodeToTarget));
		}
    }
	else
	{
	#ifdef DEBUG_LASER
		std::cout<<"Direction != NULL"<<std::endl;
	#endif
		m_jsonDoc["direction"].SetString(direction);
		gMessageHandler.Send(INVALID_RECIPIENT, RECIPIENT_NET, NET_SEND, 0, &m_jsonDoc);
		gGameMgr.SetOurTurn(false);
	}
}
 
int AttackLaserGoal::Process()
{
    ActivateIfInactive();
 
	m_status = ProcessSubgoals();
 
    return m_status;
}
 
void AttackLaserGoal::End()
{
	Clear();
}
 
void AttackLaserGoal::PrecacheJSONDocument()
{
    if(!m_jsonDoc.IsObject())
    {
        rapidjson::Value message;
        rapidjson::Value type;
        rapidjson::Value direction;
 
        direction.SetString("up");
        message.SetString("action");
        type.SetString("laser");
               
		m_jsonDoc.SetObject();

        m_jsonDoc.AddMember("message", message, m_jsonDoc.GetAllocator());
        m_jsonDoc.AddMember("type", type, m_jsonDoc.GetAllocator());
        m_jsonDoc.AddMember("direction", direction, m_jsonDoc.GetAllocator());
    }
}