#include "GraphNodes.h"
#include "SharedDefs.h"

void GraphNode::SetIndex(int index)
{
    m_index = index;
}

int GraphNode::GetIndex() const
{
    return m_index;
}