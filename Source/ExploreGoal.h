#ifndef BYTE_AI_EXPLORE_GOAL_H_
#define BYTE_AI_EXPLORE_GOAL_H_

#include "AI_Goals.h"
#include "Player.h"

/** 
 * Moves random to the closest enemy.
 * Used when the path finding algorithm
 * did not find a target.
 */
class ExploreGoal : public CompositeGoal
{
public:
	ExploreGoal(Player *owner) : m_owner(owner) {}

	/**
	 * Activate explore goal
	 */
	void Activate();

	/** 
	 * Proces goal.
	 */
	int Process();

	/**
	 * Called when end.
	 */
	void End();
private:
	Player *m_owner;
};

#endif