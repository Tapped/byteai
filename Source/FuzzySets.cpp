#include "FuzzySets.h"
#include "MathUtil.h"
#include <iostream>
#include "SharedDefs.h"

void FuzzySet::ORwithDOM(double value)
{
	if(value > m_DOM)
	{
		m_DOM = value;
	}
}

double FuzzySet_Triangle::CalculateDOM(double value) const
{
	//Handle the special case where peak is the same as value.
	if(IsEqual(m_peakPoint, value))
	{
		return 1.0;
	}
	//Check if we are left to the center.
	else if(value < m_peakPoint && value >= m_peakPoint - m_leftOffset)
	{
		double oneOverLeftOffset = 1.0 / m_leftOffset;
		return oneOverLeftOffset * (value - m_peakPoint + m_leftOffset);
	}
	//Check if we are right to the center.
	else if(value > m_peakPoint && value <= m_peakPoint + m_rightOffset)
	{
		double oneOverRightOffset = 1.0 / -m_rightOffset;
		return oneOverRightOffset * (value - m_peakPoint) + 1.0;
	}
	else
	{
		return 0.0;
	}
}

double FuzzySet_RightShoulder::CalculateDOM(double value) const
{
	//Prevent divide by zero
	if(IsEqual(m_leftOffset, 0.0) && IsEqual(value, m_peakPoint))
	{
		return 1.0;
	}

	if(value < m_peakPoint && value >= m_peakPoint - m_leftOffset)
	{
		double oneOverLeftOffset = 1.0 / m_leftOffset;
		return oneOverLeftOffset * (value - m_peakPoint + m_leftOffset);
	}
	else if(value >= m_peakPoint && value <= m_peakPoint + m_rightOffset)
	{
		return 1.0;
	}
	else
	{
		return 0.0;
	}
}

double FuzzySet_LeftShoulder::CalculateDOM(double value) const
{
	//Prevent divide by zero
	if(IsEqual(m_rightOffset, 0.0) && IsEqual(value, m_peakPoint))
	{
		return 1.0;
	}

	if(value > m_peakPoint && value <= m_peakPoint + m_rightOffset)
	{
		double oneOverRightOffset = 1.0 / -m_rightOffset;
		return oneOverRightOffset * (value - m_peakPoint) + 1.0;
	}
	else if(value <= m_peakPoint && value >= m_peakPoint - m_leftOffset)
	{
		return 1.0;
	}
	else
	{
		return 0.0;
	}
}