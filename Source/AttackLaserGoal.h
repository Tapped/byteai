#ifndef BYTEAI_ATTACK_LASER_H_
#define BYTEAI_ATTACK_LASER_H_
 
#include "AI_Goals.h"
#include<rapidjson/document.h>
#include "Player.h"
 
/**
 * Atomic goal, that fires a laser.
 */
class AttackLaserGoal : public CompositeGoal
{
public:
	AttackLaserGoal(const Player *target, const Player *attacker) : m_target(target), m_attacker(attacker) {}
       
    /**
     * Activates the goal.
     */
    void Activate();
 
    /**
     * The process would only return the status from the activate method.
    */
    int Process();
 
    /**
     * Called when the goal terminates.
     */
    void End();
private:
    static void PrecacheJSONDocument();
    static rapidjson::Document m_jsonDoc;

    const Player *m_target;
    const Player *m_attacker;
};
 
#endif