#ifndef BYTEAI_ASTAR_GRAPH_SEARCH_H_
#define BYTEAI_ASTAR_GRAPH_SEARCH_H_

#include <vector>

#include "PriorityQueue.h"
#include <list>

/**
 * Do a A* graph search.
 * Implementation is based on the book:
 * Programming Game AI by Example.
 */
template<class GRAPH_TYPE, class HEURISTIC_TYPE>
class AStarGraphSearch
{
public:
   
private:
    typedef typename GRAPH_TYPE::EdgeType Edge;
    typedef typename GRAPH_TYPE::NodeType Node;

    const GRAPH_TYPE &m_graph;

    std::vector<int> m_realCosts;//!< Real cost to any node.
    std::vector<int> m_finalCosts;//!< This is the final cost to any node.

    std::vector<const Edge*> m_shortestPathTree;//!< Stores the shortest path to any node from a root node.
    std::vector<const Edge*> m_searchFrontier;//!< Stores the edges that is on the search frontier.

    int m_sourceNode;//!< The source node, where the search starts from.
    int m_targetNode;//!< The target node, what to look after.

    /**
     * A* search algorithm.
     */
    void Search();
public:
    AStarGraphSearch(const GRAPH_TYPE &graph, int sourceNode, int targetNode) : m_graph(graph),
                                                                          m_shortestPathTree(graph.NumNodes()),
                                                                          m_searchFrontier(graph.NumNodes()),
                                                                          m_realCosts(graph.NumNodes(), 0),
                                                                          m_finalCosts(graph.NumNodes(), 0),
                                                                          m_sourceNode(sourceNode),
                                                                          m_targetNode(targetNode)
    {
        //Do the search.
        Search();
    }

    /**
     * Returns the vector of edges that the algorithm has examined.
     */
    std::vector<const Edge*> GetSPT() const
    {
        return m_shortestPathTree;
    }

    /**
     * Fills an list with node data to the specific target.
     */
    void GetPath(std::list<int> &outPath) const
    {  
        int node = m_targetNode;
        //If we did not find the target node, return.
        if(m_shortestPathTree[node] == 0)
            return;
        
        outPath.push_front(node);

        while(node != m_sourceNode && m_shortestPathTree[node] != 0)
        {
            node = m_shortestPathTree[node]->From();
            outPath.push_front(node);
        }
    }

    /**
     * Returns the cost to the target.
     */
    double GetCostToTarget() const
    {
        return m_realCosts[m_targetNode];
    }
};

template<class GRAPH_TYPE, class HEURISTIC_TYPE>
void AStarGraphSearch<GRAPH_TYPE, HEURISTIC_TYPE>::Search()
{
    IndexedPriorityQLow<int> pQueue(m_finalCosts, m_graph.NumNodes());

    pQueue.Insert(m_sourceNode);

    while(!pQueue.Empty())
    {
        int nextClosestNode = pQueue.Pop();

        m_shortestPathTree[nextClosestNode] = m_searchFrontier[nextClosestNode];

        if(nextClosestNode == m_targetNode)
        {
            return;
        }

        GRAPH_TYPE::ConstEdgeIterator constEdgeIter(m_graph, nextClosestNode);
        for(const Edge *edge = constEdgeIter.Begin();
            !constEdgeIter.End();edge = constEdgeIter.Next())
        {
			if(!edge)
			{
				break;
			}

            int realCost = m_realCosts[nextClosestNode] + edge->Cost();
            int heuristicCost = HEURISTIC_TYPE::Calculate(m_graph, m_targetNode, edge->To());

            //If we have not added the edge yet, add it.
            if(m_searchFrontier[edge->To()] == 0)
            {
                m_finalCosts[edge->To()] = realCost + heuristicCost;
                m_realCosts[edge->To()] = realCost;

                pQueue.Insert(edge->To());

                m_searchFrontier[edge->To()] = edge;
            }
            //If we found a cheaper way to this node, change the costs accordingly.
            else if(realCost < m_realCosts[edge->To()] &&
                (m_shortestPathTree[edge->To()] == 0))
            {
                m_finalCosts[edge->To()] = realCost + heuristicCost;
                m_realCosts[edge->To()] = realCost;

                pQueue.ChangePriority(edge->To());

                m_searchFrontier[edge->To()] = edge;
            }
        }
    }
}

#endif