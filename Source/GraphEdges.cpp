#include "GraphEdges.h"
#include "SharedDefs.h"

int GraphEdge::From() const
{
    return m_from;
}

void GraphEdge::SetFrom(int from)
{
    m_from = from;
}

int GraphEdge::To() const
{
    return m_to;
}

void GraphEdge::SetTo(int to)
{
    m_to = to;
}
    
int GraphEdge::Cost() const
{
    return m_cost;
}

void GraphEdge::SetCost(int cost)
{
    m_cost = cost;
}