#include "ExploreGoal.h"
#include "GameManager.h"
#include "TraverseEdgeGoal.h"
#include <iostream>

const TilePos kTilePosPossibilities[6] =
{
	TilePos(-1, -1), TilePos(1, 1), TilePos(0, -1),
	TilePos(0, 1), TilePos(-1, 0), TilePos(1, 0)
};

void ExploreGoal::Activate()
{
	m_status = GOAL_ACTIVE;

	Clear();

	const Player *player = gGameMgr.GetPlayerMgr()->FindClosestEnemy(m_owner);
	bool foundAccesibleNode = false;

	int closestNodeToTarget = INVALID_NODE_INDEX;
	int distNodeToTarget = kMaxInt;

	const Map *map = gGameMgr.GetMap();
	int fromNode = map->GetNodeIndex(m_owner->GetPosition());

    for(int i = 0;i < 6;i++)
    {
        TilePos moveTo = m_owner->GetPosition() + kTilePosPossibilities[i];
		int toNode = map->GetNodeIndex(moveTo);
		int distance = Distance(moveTo, player->GetPosition());
		if(!map->IsInaccesibleNode(toNode) && distance < distNodeToTarget)
        {
			closestNodeToTarget = toNode;
			distNodeToTarget = distance;
        }
    }

	//We are stuck if closestNodeToTarget is invalid.
	if(closestNodeToTarget != INVALID_NODE_INDEX)
	{
		AddGoal(new TraverseEdgeGoal(fromNode, closestNodeToTarget));
	}
#ifdef _DEBUG
	else
	{
		std::cout << "We are completely stuck at the moment!" << std::endl;
	}
#endif
}

int ExploreGoal::Process()
{
	ActivateIfInactive();
	m_status = ProcessSubgoals();

	return m_status;
}

void ExploreGoal::End()
{
	Clear();
}