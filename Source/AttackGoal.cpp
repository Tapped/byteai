#include "AttackGoal.h"
#include "AttackMortarGoal.h"
#include "AttackLaserGoal.h"
#include "TraverseEdgeGoal.h"
#include "GameManager.h"
#include "FollowTargetGoal.h"
#include "AttackDroidGoal.h"

const TilePos kTilePosPossibilities[6] =
{
	TilePos(-1, -1), TilePos(1, 1), TilePos(0, -1),
	TilePos(0, 1), TilePos(-1, 0), TilePos(1, 0)
};

void AttackGoal::Activate()
{
	m_status = GOAL_ACTIVE;
	
	Clear();

	AddGoal(new FollowTargetGoal(m_target, m_attacker)); 
}

int AttackGoal::Process()
{
	ActivateIfInactive();

	m_status = ProcessSubgoals();

	int distToTarget = Distance(m_target->GetPosition(), m_attacker->GetPosition());
	if(distToTarget <= LASER_RANGE_LEVEL_1)
	{
		const Weapon *weapons[2];
		weapons[0] = m_attacker->GetWeapon(0);
		weapons[1] = m_attacker->GetWeapon(1);
		//Find the best weapon
		m_weapon = weapons[0];
		double bestScore = weapons[0]->CalculateDesirability(distToTarget);

		if(weapons[1]->CalculateDesirability(distToTarget) > bestScore)
		{
			m_weapon = weapons[1];
		}

		if (m_weapon->GetRange() >= distToTarget)
		{
			switch(m_weapon->GetType())
			{
			case WPN_MORTAR:
				UseMortar(distToTarget);
				break;
			case WPN_LASER:
				UseLaser(distToTarget);
				break;
			case WPN_DROID:
				UseDroid(distToTarget);
				break;
			}
		}
	}

	return m_status;
}

void AttackGoal::End()
{
	Clear();	
}

void AttackGoal::MoveAwayFromAOE()
{
	const TilePos &attackerPos = m_attacker->GetPosition();
	const Map *map = gGameMgr.GetMap();
	int fromNode = map->GetNodeIndex(attackerPos);

	for(int i = 0;i < 6;i++)
	{
		TilePos moveTo = attackerPos + kTilePosPossibilities[i];	
		int toNode = map->GetNodeIndex(moveTo);
		if(!map->IsInaccesibleNode(toNode) && Distance(moveTo, m_target->GetPosition()) > 1)
		{
			AddGoal(new TraverseEdgeGoal(fromNode, toNode));
			std::cout << "Move away from AOE to: " << GetDirection(attackerPos, moveTo) << std::endl;
			return;
		}
	}
}

void AttackGoal::UseLaser(int distToTarget)
{
	if(distToTarget > 2)
	{
		int dir = GetDirectionAsInt(m_target->GetPosition(), m_attacker->GetPosition());

		if(dir)
		{
			TilePos currentPos = m_attacker->GetPosition();
			//Check if we hit anything with the laser ray.
			for(int i = 0;i < distToTarget - 1;i++)
			{
				currentPos += kTilePosPossibilities[dir];
				int currentNode = gGameMgr.GetMap()->GetNodeIndex(currentPos);

				//We can't shoot at stones or void, so check for that.
				//So that we can save a turn.
				if(gGameMgr.GetMap()->IsInaccesibleNode(currentNode))
				{
					return;
				}
			}
		}

		AddGoal(new AttackLaserGoal(m_target, m_attacker));
	}
}

void AttackGoal::UseMortar(int distToTarget)
{
	//If we have AOE damage, and we are too close to target, then move away and shoot.
	if(distToTarget > 1)
	{
		AddGoal(new AttackMortarGoal(m_target, m_attacker));
	}
	else
	{
		MoveAwayFromAOE();
	}
}

void AttackGoal::UseDroid(int distToTarget)
{
	//If we have AOE damage, and we are too close to target, then move away and shoot.
	if(distToTarget > 1)
	{
		AddGoal(new AttackDroidGoal(m_target, m_attacker, m_weapon->GetRange()));
	}
	else
	{
		MoveAwayFromAOE();
	}
}

bool AttackGoal::HandleMessage(Message &msg)
{
	return false;
}

