#ifndef BYTEAI_SPARSE_GRAPH_H_
#define BYTEAI_SPARSE_GRAPH_H_

#include <vector>
#include <list>
#include <iostream>
#include "SharedDefs.h"

/**
 * Sparse graph groups together nodes and edges.
 * Implementation is based on the book:
 * Programming Game AI by Example.
 */
template<class NODE_TYPE, class EDGE_TYPE>
class SparseGraph
{
public:
    typedef NODE_TYPE NodeType;
    typedef EDGE_TYPE EdgeType;

    typedef std::vector<NODE_TYPE> NodeVector;
    typedef std::list<EDGE_TYPE> EdgeList;
    typedef std::vector<EdgeList> EdgeListVector;

    SparseGraph(bool digraph) : m_nextNodeIndex(0), m_isDigraph(digraph) {}

    /**
     * Returns the node by the index.
     * Constant version.
     */
    const NodeType &GetNode(int index) const;

    /**
     * Returns the node by the index.
     * Non-const version.
     */
    NodeType &GetNode(int index);

    /**
     * Returns the edge with the specific from and to parameter.
     * Constant version.
     */
    const EdgeType &GetEdge(int from, int to) const;

    /**
     * Returns the edge with the specific from and to parameter.
     * Non-const version.
     */
    EdgeType &GetEdge(int from, int to);

    int GetNextNodeIndex() const;

    /**
     * Adds a node to the graph, and returns the index.
     */
    int AddNode(const NodeType &node);

    /**
     * Remove node from graph.
     */
    void RemoveNode(int node);

    /**
     * Add edge to graph.
     */
    void AddEdge(const EdgeType &edge);

    /**
     * Remove edge from graph.
     */
    void RemoveEdge(int from, int to);

    /** 
     * Returns the number of nodes.
     */
    int NumNodes() const;

    /**
     * Returns the number of active nodes.
     */
    int NumActiveNodes() const;

    /**
     * Returns the number of edges.
     */
    int NumEdges() const;
    
    /**
     * Returns true if this graph is directed.
     */
    bool IsDigraph() const {return m_isDigraph;}
    
    /**
     * Returns true if this graph does not contain any nodes.
     */
    bool IsEmpty() const {return m_nextNodeIndex <= 0;}

    /**
     * Returns true if the node exist in graph.
     */
    bool IsPresent(int node) const;

    /**
     * Clear the whole graph.
     */
    void Clear();

    class EdgeIterator
    {
    public:
        EdgeIterator(SparseGraph<NodeType, EdgeType> &graph, int node) : m_graph(graph), m_nodeIndex(node)
        {
            m_currentEdge = m_graph.m_edges[m_nodeIndex].begin();
        }

        EdgeType *Begin()
        {
			if(m_graph.m_edges[m_nodeIndex].size() < 1)
			{
				return 0;
			}

            m_currentEdge = m_graph.m_edges[m_nodeIndex].begin();
            return &(*m_currentEdge);
        }

        EdgeType *Next()
        {
            ++m_currentEdge;
            if(End())
            {
                return 0;
            }

            return &(*m_currentEdge);
        }

        bool End()
        {
            return m_currentEdge == m_graph.m_edges[m_nodeIndex].end();
        }
    private:
        SparseGraph<NodeType, EdgeType> &m_graph;
        const int m_nodeIndex;
        typename EdgeList::iterator m_currentEdge;
    };
    
    friend EdgeIterator;

    class ConstEdgeIterator
    {
    public:
        ConstEdgeIterator(const SparseGraph<NodeType, EdgeType> &graph, int node) : m_graph(graph), m_nodeIndex(node)
        {
            m_currentEdge = m_graph.m_edges[m_nodeIndex].begin();
        }

        const EdgeType *Begin()
        {
			if(m_graph.m_edges[m_nodeIndex].size() < 1)
			{
				return 0;
			}

            m_currentEdge = m_graph.m_edges[m_nodeIndex].begin();

            return &(*m_currentEdge);
        }

        const EdgeType *Next()
        {
            ++m_currentEdge;
            if(End())
            {
                return 0;
            }

            return &(*m_currentEdge);
        }

        bool End()
        {
            return m_currentEdge == m_graph.m_edges[m_nodeIndex].end();
        }
    private:
        const SparseGraph<NodeType, EdgeType> &m_graph;
        const int m_nodeIndex;
        typename EdgeList::const_iterator m_currentEdge;
    };
    friend ConstEdgeIterator;

    class NodeIterator
    {
    public:
        NodeIterator(SparseGraph<NodeType, EdgeType> &graph, int node) : m_graph(graph), m_nodeIndex(node)
        {
            m_currentNode = m_graph[m_nodeIndex].m_nodes.begin();
        }

        NodeType *Begin()
        {
            m_currentNode = m_graph[m_nodeIndex].m_nodes.begin();

            NextValidNode(m_currentNode);

            return &(*m_currentNode);
        }

        NodeType *Next()
        {
            ++m_currentNode;
            if(End())
            {
                return 0;
            }

            NextValidNode(m_currentNode);

            return &(*m_currentNode);
        }

        bool End()
        {
            return m_currentNode == m_graph.m_nodes.end();
        }
    private:
        //If we remove a node while iterating through the vector
        //then we need to skip these nodes.
        void NextValidNode(typename NodeVector::iterator curNode)
        {
            if(curNode == m_graph.m_nodes.end() || curNode->GetIndex() != INVALID_NODE_INDEX)
                return;

            while(curNode->GetIndex() == INVALID_NODE_INDEX)
            {
                ++curNode;

                if(curNode == m_graph.m_nodes.end())
                {
                    break;
                }
            }
        }

        SparseGraph<NodeType, EdgeType> &m_graph;
        typename NodeVector::iterator m_currentNode;
    };
    friend NodeIterator;

    class ConstNodeIterator
    {
    public:
        ConstNodeIterator(const SparseGraph<NodeType, EdgeType> &graph, int node) : m_graph(graph), m_nodeIndex(node)
        {
            m_currentNode = m_graph[m_nodeIndex].m_nodes.begin();
        }

        const NodeType *Begin()
        {
            m_currentNode = m_graph[m_nodeIndex].m_nodes.begin();

            NextValidNode(m_currentNode);

            return &(*m_currentNode);
        }

        const NodeType *Next()
        {
            ++m_currentNode;
            if(End())
            {
                return 0;
            }

            NextValidNode(m_currentNode);

            return &(*m_currentNode);
        }

        bool End()
        {
            return m_currentNode == m_graph.m_nodes.end();
        }
    private:
        //If we remove a node while iterating through the vector
        //then we need to skip these nodes.
        void NextValidNode(typename NodeVector::const_iterator curNode)
        {
            if(curNode == m_graph.m_nodes.end() || curNode->GetIndex() != INVALID_NODE_INDEX)
                return;

            while(curNode->GetIndex() == INVALID_NODE_INDEX)
            {
                ++curNode;

                if(curNode == m_graph.m_nodes.end())
                {
                    break;
                }
            }
        }

        const SparseGraph<NodeType, EdgeType> &m_graph;
        typename NodeVector::const_iterator m_currentNode;
    };
    friend ConstNodeIterator;

private:
    NodeVector m_nodes;//!< Stores the nodes.
    EdgeListVector m_edges;//!< Stores the adjacency edges for each node. Use the node index to key into vector.

    int m_nextNodeIndex;//!< Index of next node to be added.

    bool m_isDigraph;//!< Is this a directed graph?
};

template<class NODE_TYPE, class EDGE_TYPE>
const NODE_TYPE &SparseGraph<NODE_TYPE, EDGE_TYPE>::GetNode(int index) const
{
    Assert(m_nextNodeIndex > index && index != INVALID_NODE_INDEX 
        && "SparseGraph::GetNode() Index is invalid.");
    return m_nodes.at(index);
}

template<class NODE_TYPE, class EDGE_TYPE>
NODE_TYPE &SparseGraph<NODE_TYPE, EDGE_TYPE>::GetNode(int index)
{
    Assert(m_nextNodeIndex > index && index != INVALID_NODE_INDEX
        && "SparseGraph::GetNode() Index is invalid.");
    return m_nodes[index];
}

template<class NODE_TYPE, class EDGE_TYPE>
EDGE_TYPE &SparseGraph<NODE_TYPE, EDGE_TYPE>::GetEdge(int from, int to)
{
    Assert(m_nextNodeIndex > from && from != INVALID_NODE_INDEX &&
        "SparseGraph::GetEdge() invalid 'from' node.");
    Assert(m_nextNodeIndex > to && to != INVALID_NODE_INDEX &&
        "SparseGraph::GetEdge() invalid 'to' node.");

    EdgeList &edgeList = m_edges[from];
    for(EdgeList::iterator iter = edgeList.begin();
        iter != edgeList.end();++iter)
    {
        if(iter->To() == to)
        {
            return (*iter);
        }
    }
    
    Assert(false && "SparseGraph::GetEdge(): Could not find edge!");
}

template<class NODE_TYPE, class EDGE_TYPE>
const EDGE_TYPE &SparseGraph<NODE_TYPE, EDGE_TYPE>::GetEdge(int from, int to) const
{
    Assert(m_nextNodeIndex > from && from != INVALID_NODE_INDEX &&
        "SparseGraph::GetEdge() invalid 'from' node.");
    Assert(m_nextNodeIndex > to && to != INVALID_NODE_INDEX &&
        "SparseGraph::GetEdge() invalid 'to' node.");
    
    EdgeList &edgeList = m_edges[from];
    for(EdgeList::const_iterator iter = edgeList.begin();
        iter != edgeList.end();++iter)
    {
        if(iter->To() == to)
        {
            return (*iter);
        }
    }

    Assert(false && "SparseGraph::GetEdge(): Could not find edge!");
}

template<class NODE_TYPE, class EDGE_TYPE>
int SparseGraph<NODE_TYPE, EDGE_TYPE>::GetNextNodeIndex() const
{
    return m_nextNodeIndex;
}

template<class NODE_TYPE, class EDGE_TYPE>
int SparseGraph<NODE_TYPE, EDGE_TYPE>::AddNode(const NODE_TYPE &node)
{
    if(node.GetIndex() < m_nextNodeIndex)
    {
        Assert(m_nodes[node.GetIndex()].GetIndex() == INVALID_NODE_INDEX
            && "SparseGraph::AddNode() could not replace the node.");

        m_nodes[node.GetIndex()] = node;

        return m_nextNodeIndex;
    }
    else
    {
        Assert(node.GetIndex() == m_nextNodeIndex &&
            "SparseGraph::AddNode() could not add node, since it had an invalid index.");
        
        m_nodes.push_back(node);
        m_edges.resize(++m_nextNodeIndex);
        return m_nextNodeIndex;   
    }
}
 
template<class NODE_TYPE, class EDGE_TYPE>
void SparseGraph<NODE_TYPE, EDGE_TYPE>::RemoveNode(int node)
{
    Assert(node < m_nextNodeIndex && "SparseGraph::RemoveNode() Invalid node index.");
    m_nodes[node].SetIndex(INVALID_NODE_INDEX);

    if(!m_isDigraph)
    {
        //Remove all neighbour edges leading to this node.
        for(EdgeList::iterator fromEdge = m_edges[node].begin();
            fromEdge != m_edges[node].end();++fromEdge)
        {
            for(EdgeList::iterator toEdge = m_edges[fromEdge->To()].begin();
                toEdge != m_edges[fromEdge->To()];++toEdge)
            {
                if(toEdge->To() == node)
                {
                    m_edges.erase(toEdge);

                    break;
                }
            }
        }

        m_edges[node].clear();
    }
    else
    {
        //Find every invalid node in the edge list and remove the edge.
        for(EdgeListVector::iterator edgeList = m_edges.begin();
            edgeList != m_edges.end();++edgeList)
        {
            for(EdgeList::iterator edge = edgeList->begin();
                edge != edgeList->end();++edge)
            {
                if(m_nodes[edge->To()].GetIndex() == INVALID_NODE_INDEX ||
                    m_nodes[edge->From()].GetIndex() == INVALID_NODE_INDEX)
                {
                    edge = edgeList->erase(edge);
                }
            }
        }
    }
}

template<class NODE_TYPE, class EDGE_TYPE>
void SparseGraph<NODE_TYPE, EDGE_TYPE>::AddEdge(const EDGE_TYPE &type)
{
    Assert(type.From() < m_nextNodeIndex && type.From() != INVALID_NODE_INDEX &&
        "SparseGraph::AddEdge() edge.from is invalid.");

    m_edges[type.From()].push_back(type);

    //We need to add an extra edge if this is a undirected graph.
    if(!m_isDigraph)
    {
        EDGE_TYPE edge(type.To(), type.From());

        m_edges[edge.From()].push_back(edge);
    }
}

template<class NODE_TYPE, class EDGE_TYPE>
void SparseGraph<NODE_TYPE, EDGE_TYPE>::RemoveEdge(int from, int to)
{
    Assert(m_nextNodeIndex > from && from != INVALID_NODE_INDEX &&
        "SparseGraph::GetEdge() invalid 'from' node.");
    Assert(m_nextNodeIndex > to && to != INVALID_NODE_INDEX &&
        "SparseGraph::GetEdge() invalid 'to' node.");

    EdgeList &edgeList = m_edges[from];
    for(EdgeList::iterator iter = edgeList.begin();
        iter != edgeList.end();++iter)
    {
        if(iter->To() == to)
        {
            edgeList.erase(iter);
            break;
        }
    }

    if(!m_isDigraph)
    {
        EdgeList &edgeList = m_edges[to];
        for(EdgeList::iterator iter = edgeList.begin();
        iter != edgeList.end();++iter)
        {
            if(iter->To() == from)
            {
                edgeList.erase(iter);
                return;
            }
        }
    }
}

template<class NODE_TYPE, class EDGE_TYPE>
int SparseGraph<NODE_TYPE, EDGE_TYPE>::NumNodes() const
{
    return m_nextNodeIndex;
}

template<class NODE_TYPE, class EDGE_TYPE>
int SparseGraph<NODE_TYPE, EDGE_TYPE>::NumActiveNodes() const
{
    int numActiveNodes = 0;
    for(NodeVector::const_iterator iter = m_nodes.begin();
        iter != m_nodes.end();++iter)
    {
        if(iter->GetIndex() != INVALID_NODE_INDEX)
        {
            ++numActiveNodes;
        }
    }

    return numActiveNodes;
}

template<class NODE_TYPE, class EDGE_TYPE>
int SparseGraph<NODE_TYPE, EDGE_TYPE>::NumEdges() const
{
    int numEdges = 0;
    for(EdgeListVector::iterator edgeIter = m_edges.begin();
        edgeIter != m_edges.end();++edgeIter)
    {
        numEdges += edgeIter->size();
    }

    return numEdges;
}

template<class NODE_TYPE, class EDGE_TYPE>
void SparseGraph<NODE_TYPE, EDGE_TYPE>::Clear()
{
    m_edges.clear();
    m_nodes.clear();
    m_nextNodeIndex = 0;
}

template<class NODE_TYPE, class EDGE_TYPE>
bool SparseGraph<NODE_TYPE, EDGE_TYPE>::IsPresent(int node) const
{
    return node < m_nextNodeIndex && m_nodes[node] != INVALID_NODE_INDEX;
}

#endif