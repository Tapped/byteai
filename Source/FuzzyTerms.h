#ifndef BYTEAI_FUZZY_TERMS_H_
#define BYTEAI_FUZZY_TERMS_H_

#include "FuzzySets.h"

/** 
 * Base class for Fuzzy terms.
 */
class FuzzyTerm
{
public:
	/** 
	 * Clone the fuzzy term.
	 * @return A fuzzy term pointer, remember to DELETE this.
	 */
	virtual FuzzyTerm *Clone() = 0;

	/**
	 * @return Degree of membership.
	 */
	virtual double GetDOM() const = 0;

	/**
	 * Clear Degree of membership.
	 */
	virtual void ClearDOM() = 0;

	/**
	 * OR a value with the degree of membership.
	 */
	virtual void ORwithDOM(double val)=0;
};

/**
 * A proxy class for fuzzy sets.
 * It is a proxy class, since we want to have the Fuzzy Term interface
 * but in the same time have the FuzzySet interface.
 */
class FzSet : public FuzzyTerm
{
public:
	FzSet(FuzzySet *set) : m_fuzzySet(set) {}

	FuzzyTerm *Clone();

	double GetDOM() const;

	void ClearDOM();

	void ORwithDOM(double val);

	void SetFuzzySet(FuzzySet *set);
private:
	FuzzySet *m_fuzzySet;
};

/**
 * AND two fuzzy sets.
 */
class FzAnd : public FuzzyTerm
{
public:
    ~FzAnd();

    FzAnd(FuzzyTerm &setA, FuzzyTerm &setB);

    FuzzyTerm *Clone();

    /**
     * Returns the two fuzzy sets AND'ed DOM's.
     */
    double GetDOM() const;

	void ClearDOM();

	void ORwithDOM(double val);
private:
    FuzzyTerm *m_pSetA;
    FuzzyTerm *m_pSetB;
};

#endif