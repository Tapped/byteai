#ifndef BYTEAI_MESSAGE_HANDLER_H_
#define BYTEAI_MESSAGE_HANDLER_H_

#include "MathUtil.h"
#include "Recipient.h"
#include <set>
#include <map>

/**
 * Message handler manages the local messages, sent between systems.
 */
class MessageHandler
{
public:
	~MessageHandler() {}

	/**
	 * Sends a message to a recipient.
	 * @param from Who sent the message?
	 * @param to Who should receive it?
	 * @param message What is the message?
	 * @param delay When should the message be dispatched?(millisec)
	 * @param extraData Is there some more information?
	 */
	void Send(uint from, uint to, uint message, uint delay = 0, void *extraData = 0);

	/**
	 * Resolve the message queue.
	 */
	void Update();

	/**
	 * Add a recipient.
	 */
	void AddRecipient(Recipient *recipient);

	/**
	 * Remove a recipient.
	 */
	void RemoveRecipient(Recipient *recipient);

	/**
	 * @return A recipient by ID.
	 */
	Recipient  *GetRecipient(uint id) const;

	static MessageHandler &GetInstance()
	{
		static MessageHandler instance;
		return instance;
	}
private:
	MessageHandler() {}
	MessageHandler(const MessageHandler&) {}
	MessageHandler &operator=(const MessageHandler&) {}

	/**
	 * Discharges a message to a recipient.
	 */
	bool Discharge(Recipient *recipient, Message &msg);

	typedef std::map<int, Recipient*>::const_iterator ReciepientsIter;
	std::set<Message> m_messages;
	std::map<int, Recipient*> m_reciepients;
};

#define gMessageHandler MessageHandler::GetInstance()

#endif