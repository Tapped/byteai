#include "FuzzyTerms.h"
#include "FuzzySets.h"
#include "SharedDefs.h"
#include "MathUtil.h"

FuzzyTerm *FzSet::Clone()
{
	return new FzSet(*this);
}

double FzSet::GetDOM() const
{
	return m_fuzzySet->GetDOM();
}

void FzSet::ClearDOM()
{
	m_fuzzySet->ClearDOM();
}

void FzSet::ORwithDOM(double value)
{
	m_fuzzySet->ORwithDOM(value);
}

void FzSet::SetFuzzySet(FuzzySet *set)
{
	Assert(set && "FzSet::SetFuzzySet() NULL pointer is not allowed.");
	m_fuzzySet = set;
}

FzAnd::~FzAnd()
{
    SAFE_DELETE(m_pSetA);
    SAFE_DELETE(m_pSetB);
}
FzAnd::FzAnd(FuzzyTerm &setA, FuzzyTerm &setB)
{
    m_pSetA = static_cast<FuzzyTerm*>(setA.Clone());
    m_pSetB = static_cast<FuzzyTerm*>(setB.Clone());
}

FuzzyTerm *FzAnd::Clone()
{
    return new FzAnd(*m_pSetA, *m_pSetB);
}

double FzAnd::GetDOM() const
{
    return min(m_pSetA->GetDOM(), m_pSetB->GetDOM());
}

void FzAnd::ClearDOM()
{
    m_pSetA->ClearDOM();
    m_pSetB->ClearDOM();
}

void FzAnd::ORwithDOM(double value)
{
    m_pSetA->ORwithDOM(value);
    m_pSetB->ORwithDOM(value);
}