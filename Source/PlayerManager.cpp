#include "PlayerManager.h"
#include "SharedDefs.h"
#include "MessageHandler.h"
#include <iostream>

PlayerManager::~PlayerManager()
{
}

void PlayerManager::Init()
{
    gMessageHandler.AddRecipient(this);
}

bool PlayerManager::HandleMessage(Message &msg)
{
    switch(msg.Msg)
    {
    case PLAYERS_UPDATE:
        UpdatePlayers(msg);
        return true;
        break;
    default:
        std::cout << "Unknown message received in " << __FILE__ " at line " << __LINE__ << std::endl;
        break;
    }

    return false;
}

void PlayerManager::UpdatePlayers(Message &msg)
{
    ExtraData_Players *playersData = (ExtraData_Players*)msg.ExtraData;
    m_numPlayers = playersData->numPlayers;

    for(int i = 0;i < m_numPlayers;i++)
    {
        m_players[playersData->players[i].name].SetPlayerData(&playersData->players[i]);
    }

    if(!m_pOurPlayer)
    {
        m_pOurPlayer = &m_players[AI_NAME];
    }
}

Player *PlayerManager::GetOurPlayer()
{
    Assert(m_pOurPlayer && "PlayerManager::GetOutPlayer() our player is not defined yet!");
    return m_pOurPlayer;
}

const Player* PlayerManager::FindClosestEnemy(const Player *owner) const
{
	std::map<std::string, int> distances;
	int shortestDistance = kMaxInt;
	const Player *closestPlayer;

	for(std::map<std::string, Player>::const_iterator it=m_players.begin(); it!=m_players.end(); ++it) 
	{
		int distance = Distance(m_pOurPlayer->GetPosition(), it->second.GetPosition());
		
		if(&it->second != owner)
		{
			if(distance < shortestDistance)
			{
				shortestDistance = distance;
				closestPlayer = &it->second;
			}
		}

	}

	return closestPlayer;
}