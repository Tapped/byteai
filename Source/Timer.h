#ifndef BYTEAI_TIMER_H_
#define BYTEAI_TIMER_H_

#include <Windows.h>
#include "MathUtil.h"

/**
 * Timer for time dependent code.
 */
class Timer
{
public:
    Timer(){Reset();}

    /**
     * Initialize timer. NOTE: This function should only be called once!
     */
    static bool Init()
    {
        if(!QueryPerformanceFrequency(&m_ticksPerSecond))
        {
            return false;
        }

        m_realFreq = 1.0 / (double)m_ticksPerSecond.QuadPart;

        GlobalReset();

        return true;
    }

    /**
     * Resets the global clock.
     */
    static void GlobalReset()
    {
        QueryPerformanceCounter(&m_gStartTime);
        m_gElapsedTime = m_gStartTime;
    }

    /**
     * Elapsed time from last call to this function, or other functions which also use elapsed time.
     * @return Real number in seconds.
     */
    static double GetGlobalElapsedTime()
    {
        LARGE_INTEGER currentTime;
        QueryPerformanceCounter(&currentTime);

        double sec = (double)(currentTime.QuadPart - m_gElapsedTime.QuadPart) * m_realFreq;
        m_gElapsedTime = currentTime;
        return sec;
    }

    /**
     * Elapsed ticks from last call to thic or other function which also use elapsed time.
     * @return int64(DWORD, long long int) ticks.
     */
    static LONGLONG GetGlobalElapsedTicks()
    {
        LARGE_INTEGER currentTime;
        QueryPerformanceCounter(&currentTime);
        LONGLONG ticks = currentTime.QuadPart - m_gElapsedTime.QuadPart;
        m_gElapsedTime = currentTime;

        return ticks;
    }

    /**
     * Elapsed ticks from last call to this or other functions, which also use elapsed time.
     * @return int64(DWORD, long long int) ticks.
     */
    static LONGLONG GetGlobalElapsedMillisec()
    {
        LARGE_INTEGER currentTime;
        QueryPerformanceCounter(&currentTime);
        double milliSec = (double)(currentTime.QuadPart - m_gElapsedTime.QuadPart) / (double)m_ticksPerSecond.QuadPart;

        m_gElapsedTime = currentTime;

        return (LONGLONG)(milliSec * 1000);
    }

    /**
     * Time since last reset of global clock.
     * @return unsigned int ticks.
     */
    static LONGLONG GetGlobalMillisec()
    {
        LARGE_INTEGER currentTime;
        QueryPerformanceCounter(&currentTime);
        double milliSec = (double)(currentTime.QuadPart - m_gStartTime.QuadPart) / (double)m_ticksPerSecond.QuadPart;
        milliSec *= 1000;

        return (LONGLONG)(milliSec * 1000);
    }

	static uint GetGlobalMillisecUINT()
	{
		LARGE_INTEGER currentTime;
        QueryPerformanceCounter(&currentTime);
        double milliSec = (double)(currentTime.QuadPart - m_gStartTime.QuadPart) / (double)m_ticksPerSecond.QuadPart;

		return (uint)(milliSec * 1000);
	}

    /**
     * Reset this instance of timer.
     */
    void Reset()
    {
        QueryPerformanceCounter(&m_startTime);
        m_elapsedTime = m_startTime;
    }

    /**
     * Elapsed time from last call to this function, or other functions which also use elapsed time.
     * @return Real number in seconds.
     */
    double GetElapsedTime()
    {
        LARGE_INTEGER currentTime;
        QueryPerformanceCounter(&currentTime);

        double sec = (double)(currentTime.QuadPart - m_elapsedTime.QuadPart) * m_realFreq;
        m_elapsedTime = currentTime;
        return sec;
    }

    /**
     * Elapsed ticks from last call to this or other function which also use elapsed time.
     * @return int64(DWORD, long long int) ticks.
     */
    LONGLONG GetElapsedTicks()
    {
        LARGE_INTEGER currentTime;
        QueryPerformanceCounter(&currentTime);
        LONGLONG ticks = currentTime.QuadPart - m_elapsedTime.QuadPart;
        m_elapsedTime = currentTime;

        return ticks;
    }

    /**
     * Time since last call to this or other functions, which also use elapsed time.
     * @return int64(DWORD, long long int) ticks.
     */
    LONGLONG GetElapsedMillisec()
    {
        LARGE_INTEGER currentTime;
        QueryPerformanceCounter(&currentTime);
        double milliSec = (double)(currentTime.QuadPart - m_elapsedTime.QuadPart) / (double)m_ticksPerSecond.QuadPart;

        m_elapsedTime = currentTime;

        return (LONGLONG)(milliSec * 1000);
    }

    /**
     * Time since last reset of this instance.
     * @return int64(DWORD, long long int) ticks.
     */
    LONGLONG GetMillisec()
    {
        LARGE_INTEGER currentTime;
        QueryPerformanceCounter(&currentTime);
        double milliSec = (double)(currentTime.QuadPart - m_startTime.QuadPart) / (double)m_ticksPerSecond.QuadPart;

        return (LONGLONG)(milliSec * 1000);
    }

    static LARGE_INTEGER m_gStartTime;//!< When the global timer was last reset.
    static LARGE_INTEGER m_ticksPerSecond;//!< Number of ticks per second, in other words the frequency.
    static LARGE_INTEGER m_gElapsedTime;//!< Time elapsed between each call.
    static double m_realFreq;//!< Floating/Double point frequency.

    LARGE_INTEGER m_startTime;//!< When this instance was last reset.
    LARGE_INTEGER m_elapsedTime;//!< Time elapsed between each call.
};

#endif