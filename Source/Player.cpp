#include <iostream>
#include "Player.h"
#include "MessageHandler.h"
#include "SharedDefs.h"
#include <rapidjson/document.h>
#include "GameManager.h"

Player::~Player()
{
    SAFE_DELETE(m_pWeapons[0]);
    SAFE_DELETE(m_pWeapons[1]);
}

Player::Player()
{
    m_pWeapons[0] = 0;
    m_pWeapons[1] = 0;

    m_bHaveGotInitialPos = false;
}

void Player::ChooseWeapons(int mapSize, int numScrap, int numExplosium, int numRubidium)
{
    double wpnMortarScore = WeaponMortar::CalculateDesirabilityAtStart(mapSize, numExplosium);
    double wpnLaserScore = WeaponLaser::CalculateDesirabilityAtStart(mapSize, numRubidium);
    double wpnDroidScore = WeaponDroid::CalculateDesirabilityAtStart(mapSize, numScrap);

    double bestScore = wpnMortarScore;
    double secBestScore = wpnMortarScore;
    int bestWpn = WPN_MORTAR;
    int secBestWpn = WPN_MORTAR;

    if(wpnLaserScore > bestScore)
    {
        bestScore = wpnLaserScore;
        bestWpn = WPN_LASER;
    }
    else
    {
        secBestScore = wpnLaserScore;
        secBestWpn = WPN_LASER;
    }
    
    if(wpnDroidScore > bestScore)
    {
        bestScore = wpnDroidScore;
        bestWpn = WPN_DROID;
    }
    else if(wpnDroidScore > secBestScore)
    {
        secBestScore = wpnDroidScore;
        secBestWpn = WPN_DROID;
    }

    AddWeapon(bestWpn, 0);
    AddWeapon(secBestWpn, 1);

    //Generate a loadout json script.
    rapidjson::Document doc;
    doc.SetObject();

    rapidjson::Value message;
    rapidjson::Value primWeapon;
    rapidjson::Value secWeapon;

    message.SetString("loadout");
    primWeapon.SetString(m_pWeapons[0]->GetString());
    secWeapon.SetString(m_pWeapons[1]->GetString());

    doc.AddMember("primary-weapon", primWeapon, doc.GetAllocator());
    doc.AddMember("message", message, doc.GetAllocator());
    doc.AddMember("secondary-weapon", secWeapon, doc.GetAllocator());

    gMessageHandler.Send(INVALID_RECIPIENT, RECIPIENT_NET, NET_SEND, 0, &doc);
}

void Player::IncrementNumResources(char type)
{
	if(m_numResources.find(type) == m_numResources.end())
	{
		m_numResources[type] = 0;
	}
	else
	{
		++m_numResources[type];
	}
}

void Player::UpgradeWeapons()
{
	char primResource;
	if(GetWeapon(0)->GetType() == WPN_MORTAR)
	{
		primResource = TILE_EXPLOSIUM;
	}
	else if(GetWeapon(0)->GetType() == WPN_LASER)
	{
		primResource = TILE_RUBIDIUM;
	}
	else if(GetWeapon(0)->GetType() == WPN_DROID)
	{
		primResource = TILE_SCRAP;
	}
	
	char secondResource;
	if(GetWeapon(1)->GetType() == WPN_MORTAR)
	{
		secondResource = TILE_EXPLOSIUM;
	}
	else if(GetWeapon(1)->GetType() == WPN_LASER)
	{
		secondResource = TILE_RUBIDIUM;
	}
	else if(GetWeapon(1)->GetType() == WPN_DROID)
	{
		secondResource = TILE_SCRAP;
	}
	
	/*for(std::map<char, int>::iterator iter = m_numResources.begin();iter != m_numResources.end(); iter++)
	{
		if(m_pWeapons iter->second > 
	}*/
}

void Player::AddWeapon(int wpnType, int slot)
{
    if(m_pWeapons[slot])
    {
        SAFE_DELETE(m_pWeapons[slot]);
    }

    switch(wpnType)
    {
    case WPN_DROID:
        m_pWeapons[slot] = new WeaponDroid();
        break;
    case WPN_LASER:
        m_pWeapons[slot] = new WeaponLaser();
        break;
    case WPN_MORTAR:
        m_pWeapons[slot] = new WeaponMortar();
        break;
    default:
        std::cout << "Invalid weapon type at line " << __LINE__ << std::endl;
        break;
    }
}

void Player::SetPlayerData(const PlayerData *playerData)
{
    Assert(playerData && "Player::SetPlayerData() did not expect that playerData is a NULL pointer");
    m_playerData = *playerData;

	if(m_bHaveGotInitialPos)
	{
		SetWeapons();
		//If we are the player and we are dead
		if(this == gGameMgr.GetPlayer() && m_playerData.health <= 0)
		{
			//Upate the client position to spawn.
			m_clientPosition = m_spawnPos;
		}
	}

#if !NDEBUG
    if(m_bHaveGotInitialPos && this == gGameMgr.GetPlayer())
    {
        Assert(m_clientPosition == m_playerData.position && "Misspredicted position!");
    }
#endif
    if(gGameMgr.HasGameStarted())
    {
         m_bHaveGotInitialPos = true;
    }

	if(!m_bHaveGotInitialPos)
	{
		m_spawnPos = m_playerData.position;
	}

	//Update our predicted position.
	m_clientPosition = m_playerData.position;
}

void Player::SetWeapons()
{
	if(!m_pWeapons[0])
	{
		AddWeapon(GetWpnTypeFromString(m_playerData.primWeapon), 0);
	}
	
	if(!m_pWeapons[1])
	{
		AddWeapon(GetWpnTypeFromString(m_playerData.primWeapon), 1);
	}

	m_pWeapons[0]->SetLevel(m_playerData.levelPrimWeapon);
	m_pWeapons[1]->SetLevel(m_playerData.levelSecondWeapon);
}

const PlayerData *Player::GetPlayerData() const
{
    return &m_playerData;
}

const TilePos &Player::GetPosition() const
{
    return m_clientPosition;
}

void Player::SetPosition(const TilePos &pos)
{
    m_clientPosition = pos;
}

const Weapon *Player::GetWeapon(int weaponIdx) const
{
	return m_pWeapons[weaponIdx];
}
