#include "EvaluateMine.h"
#include "GameManager.h"
#include "MineGoal.h"
#include "PlayerManager.h"
#include "Player.h"
#include "Messages.h"
#include "Map.h"
#include "Weapons.h"

const double kHealtFactor = 7;

double EvaluateMine::CalculateDesirability()
{
	const Map *map = gGameMgr.GetMap();
	const PlayerManager *playerMgr = gGameMgr.GetPlayerMgr();
	/*
		- Wanted resource types
		- Distance to wanted resource type

	*/

	m_attacker = gGameMgr.GetPlayer();
	int primResource, secondResource;
	if(m_attacker->GetWeapon(0)->GetType() == WPN_MORTAR)
	{
		primResource = TILE_EXPLOSIUM;
	}
	else if(m_attacker->GetWeapon(0)->GetType() == WPN_LASER)
	{
		primResource = TILE_RUBIDIUM;
	}
	else if(m_attacker->GetWeapon(0)->GetType() == WPN_DROID)
	{
		primResource = TILE_SCRAP;
	}

	if(m_attacker->GetWeapon(1)->GetType() == WPN_MORTAR)
	{
		secondResource = TILE_EXPLOSIUM;
	}
	else if(m_attacker->GetWeapon(1)->GetType() == WPN_LASER)
	{
		secondResource = TILE_RUBIDIUM;
	}
	else if(m_attacker->GetWeapon(1)->GetType() == WPN_DROID)
	{
		secondResource = TILE_SCRAP;
	}

	int secondResourceDist = map->MinDistanceToATileType(m_attacker->GetPosition(), secondResource);
	int primResourceDist = map->MinDistanceToATileType(m_attacker->GetPosition(), primResource);	

	//Find average levels for primary and secondary weapons for all enemies
	PlayerManager::ConstPlayerIter it(*playerMgr);
	double averagePrimaryWpnLevel = 0;
	double averageSecondaryWpnLevel = 0;
	for(const Player *player = it.Begin(); !it.End(); player = it.Next())
	{
		if(player != gGameMgr.GetPlayer())
		{
			averagePrimaryWpnLevel += player->GetPlayerData()->levelPrimWeapon;
			averageSecondaryWpnLevel += player->GetPlayerData()->levelSecondWeapon;
		}
	}
	averagePrimaryWpnLevel = averagePrimaryWpnLevel/(playerMgr->NumPlayers()-1);
	averageSecondaryWpnLevel = averageSecondaryWpnLevel/(playerMgr->NumPlayers()-1);
	
	//Calculate factors for each weapon
	double secondWpnFactor = 0;
	double primWpnFactor = 0;
	if(averagePrimaryWpnLevel > m_attacker->GetPlayerData()->levelPrimWeapon)
	{
		primWpnFactor = (averagePrimaryWpnLevel - (double)m_attacker->GetPlayerData()->levelPrimWeapon)/(double)primResourceDist;
	}
	if(averageSecondaryWpnLevel > m_attacker->GetPlayerData()->levelSecondWeapon)
	{
		secondWpnFactor = (averageSecondaryWpnLevel - (double)m_attacker->GetPlayerData()->levelSecondWeapon)/(double)secondResourceDist;
	}

	double healthDistFactor =  (double)secondResourceDist / (double)m_attacker->GetPlayerData()->health;
	healthDistFactor *= kHealtFactor;

	//Set target tile type
	double wpnFactor = 0;
	if(primWpnFactor > secondWpnFactor || primResourceDist >= secondResourceDist)
	{
		m_targetType = primResource;
		wpnFactor = primWpnFactor;
	}else if (secondWpnFactor > primWpnFactor || secondResourceDist > primResourceDist)
	{
		m_targetType = secondResource;
		wpnFactor = secondWpnFactor;
	}
		
	//return min((wpnFactor + healthDistFactor) * 100, 100);
	return 0.0;
}

MineGoal *EvaluateMine::GetGoal()
{
	return new MineGoal(m_attacker, m_targetType);
}