#ifndef BYTEAI_GRAPH_NODES_H_
#define BYTEAI_GRAPH_NODES_H_

#include "TilePos.h"

/**
 * Graph node is self-explained...
 * Implementation is based on the book:
 * Programming Game AI by Example.
 */
class GraphNode
{
public:
    GraphNode() : m_index(0) {}
    GraphNode(int index) : m_index(index) {}

    virtual ~GraphNode() {}

    void SetIndex(int index);
    int GetIndex() const;
protected:
    int m_index;//Graph node index.
};

class NavGraphNode : public GraphNode
{
public:
    NavGraphNode() {}

    NavGraphNode(int index) : GraphNode(index) {}

    NavGraphNode(int index, int j, int k) : GraphNode(index), m_position(j, k) {}

    const TilePos &GetPosition() const
    {
        return m_position;
    }

    void SetPosition(const TilePos &position)
    {
        m_position = position;
    }
private:
    TilePos m_position;
};

#endif