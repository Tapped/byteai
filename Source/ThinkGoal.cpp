#include "ThinkGoal.h"
#include "GameManager.h"
#include "AttackGoal.h"
#include "ExploreGoal.h"
#include "EvaluateAttack.h"
void ThinkGoal::Activate()
{
    m_status = GOAL_ACTIVE;

    Clear();
	
	//std::cout<<"ATTACK FACTOR: " << evaluateAttack.CalculateDesirability() << std::endl;
	//std::cout<<"MINE FACTOR: " << evaluateMine.CalculateDesirability() << std::endl;
	if(evaluateAttack.CalculateDesirability() >= evaluateMine.CalculateDesirability())
	{
		//std::cout<<"ATTACK!"<<std::endl;
		AddGoal(evaluateAttack.GetGoal());
	}
	else
	{
		//std::cout<<"MINE!"<<std::endl;
		AddGoal(evaluateMine.GetGoal());
	}
	/*This code is only for testing, therefore not optimized.
	const TilePos &playerPos = gGameMgr.GetPlayer()->GetPosition();
	PlayerManager::ConstPlayerIter playerIterator(*gGameMgr.GetPlayerMgr());

	for(const Player *player = playerIterator.Begin();!playerIterator.End();player = playerIterator.Next())
	{
		if(gGameMgr.GetPlayer() != player)
		{
			//Attack the specific target.
			AddGoal(new AttackGoal(player, gGameMgr.GetPlayer()));
		}
	}*/
}

int ThinkGoal::Process()
{
    ActivateIfInactive();

	//Process sub goals.
    m_status = ProcessSubgoals();

	if(m_status == GOAL_FAILED)
	{
		AddGoal(new ExploreGoal(gGameMgr.GetPlayer()));
		m_status = GOAL_ACTIVE;
	}

    return m_status;
}

void ThinkGoal::End()
{
}

bool ThinkGoal::HandleMessage(Message &msg)
{
    bool isMsgRead = CompositeGoal::HandleMessage(msg);
    if(!isMsgRead)
    {
        //The sub goals could not read message so we should do that.
    }

    return isMsgRead;
}
