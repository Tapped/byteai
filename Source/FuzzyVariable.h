#ifndef BYTEAI_FUZZY_VARIABLE
#define BYTEAI_FUZZY_VARIABLE

#include <map>
#include "FuzzySets.h"
#include "FuzzyTerms.h"

class FuzzyModule;

class FuzzyVariable
{
public:
	FuzzyVariable() : m_maxRange(0.0), m_minRange(0.0) {}

	/**
	 * Add a triangle set to the fuzzy variable.
	 * @param ID Identifier for the triangle set.
	 * @param minBound Is where the fuzzy set starts.
	 * @param peak Is the peak of the triangle.
	 * @param maxBound Is where the fuzzy set ends.
	 */
	FzSet AddTriangleSet(int ID, double minBound, double peak, double maxBound);

	/**
	 * Add a left shoulder set to the fuzzy variable.
	 * @param ID Identifier for the triangle set.
	 * @param minBound Is where the fuzzy set starts.
	 * @param peak Is the peak of the triangle.
	 * @param maxBound Is where the fuzzy set ends.
	 */
	FzSet AddLefShoulderSet(int ID, double minBound, double peak, double maxBound);

	/**
	 * Add a right shoulder set to the fuzzy variable.
	 * @param ID Identifier for the triangle set.
	 * @param minBound Is where the fuzzy set starts.
	 * @param peak Is the peak of the triangle.
	 * @param maxBound Is where the fuzzy set ends.
	 */
	FzSet AddRightShoulderSet(int ID, double minBound, double peak, double maxBound);

	/**
	 * Fuzzify a value by calculating it's DOM in each of the fuzzy sets.
	 */
	void Fuzzify(double value);

	/**
	 * Defuzzify the variable using the Average of Maxima algorithm. 
	 */
	double DeFuzzifyMaxAverage() const;

	/**
	 * Defuzzify the variable using the Centroid algorithm.
	 */
	double DeFuzzifyCentroid(int numSamples) const;
private:
	/**
	 * Adjusts the range to fit the min and max parameters.
	 */
	void AdjustRange(double min, double max);
	
	typedef std::map<int, FuzzySet*> FuzzySets;

	FuzzySets m_fuzzySets;///< Stores fuzzy sets in a Hash map, where the key is an ID, and the key-value is a pointer to a FuzzySet.

	double m_minRange;//!< Min range of variable.
	double m_maxRange;//!< Max range of variable. 

	//Prevent so that destructor or copy constructor
	//can't be called by the client.
	FuzzyVariable(const FuzzyVariable&);
	FuzzyVariable &operator=(const FuzzyVariable&);
	~FuzzyVariable(); 
	
	friend FuzzyModule;
};

#endif