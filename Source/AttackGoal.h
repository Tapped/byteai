#ifndef BYTEAI_ATTACK_GOAL_H_
#define BYTEAI_ATTACK_GOAL_H_

#include "AI_Goals.h"
#include "Player.h"
#include "Weapons.h"

/**
 * Attacks a target.
 * It will choose the best weapon for the situation.
 */
class AttackGoal : public CompositeGoal
{
public:
	AttackGoal(const Player *target, const Player *attacker) : m_target(target), m_attacker(attacker) {}

	/**
	 * Activates the goal, and choose the best weapon.
	 */
	void Activate();

	/**
	 * Process the goal, updating what's needs to be updated.
	 */
	int Process();

	/**
	 * End is called when this goal terminates.
	 */
	void End();

	bool HandleMessage(Message &msg);
private:
	/**
	 * Finds a valid tile that can be moved to around the attacker.
	 * Used to find the best move to avoid AOE from our own weapon.
	 */
	void MoveAwayFromAOE();

	/**
	 * Use the laser.
	 * It will check if the laser is suitable.
	 */
	void UseLaser(int distToTarget);

	/**
	 * Use the mortar.
	 * It will check if the mortar is suitable.
	 */
	void UseMortar(int distToTarget);

	/**
	 * Use droid.
	 * It will check if the droid is suitable.
	 */
	void UseDroid(int distToTarget);

	const Player *m_target;//!< The target to attack.
	const Player *m_attacker;//!< The attacker
	const Weapon *m_weapon;//!< The weapon to be fired.
};

#endif