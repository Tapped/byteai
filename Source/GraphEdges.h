#ifndef BYTEAI_GRAPH_EDGES_H_
#define BYTEAI_GRAPH_EDGES_H_

/**
 * Contains information about the edge between two nodes.
 * In other words connect information for nodes.
 * Implementation is based on the book:
 * Programming Game AI by Example.
 */
class GraphEdge
{
public:
    GraphEdge(int from, int to) : m_from(from), m_to(to), m_cost(1) {}
    GraphEdge(int from, int to, int cost) : m_from(from), m_to(to), m_cost(cost) {}

    virtual ~GraphEdge() {}

    int From() const;
    void SetFrom(int from);

    int To() const;
    void SetTo(int to);
    
    int Cost() const;
    void SetCost(int cost);
protected:
    int m_from;//!< From which node?
    int m_to;//!< To which node?
    int m_cost;//!< How much does it cost to traverse edge?
};

#endif