#ifndef BYTEAI_HEURISTIC_FUNCTIONS_H_
#define BYTEAI_HEURISTIC_FUNCTIONS_H_

#include "TilePos.h"

class HeuristicManhattan
{
public:
    template<class GRAPH_TYPE>
    static int Calculate(const GRAPH_TYPE &graph, int nodeOne, int nodeTwo)
    {
        return Distance(graph.GetNode(nodeOne).GetPosition(), graph.GetNode(nodeTwo).GetPosition());
    }
};

#endif