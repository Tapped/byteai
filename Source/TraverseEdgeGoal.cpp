#include "TraverseEdgeGoal.h"
#include "MessageHandler.h"
#include "GameManager.h"

#define UP "up"
#define DOWN "down"
#define RIGHT_UP "right-up"
#define RIGHT_DOWN "right-down"
#define LEFT_UP "left-up"
#define LEFT_DOWN "left-down"

rapidjson::Document TraverseEdgeGoal::m_cachedDoc;

void TraverseEdgeGoal::Activate()
{
    m_status = GOAL_ACTIVE;
}

int TraverseEdgeGoal::Process()
{
    ActivateIfInactive();

    const TilePos &fromTile =  gGameMgr.GetMap()->GetNode(m_from).GetPosition();
    const TilePos &toTile = gGameMgr.GetMap()->GetNode(m_to).GetPosition();

#if !NDEBUG
	Assert(!gGameMgr.GetMap()->IsInaccesibleNode(m_to));
#endif

    //Get the move direction.
    const char *szMoveDir = GetDirection(fromTile, 
                                toTile);

	if(szMoveDir != 0)
	{
		GenerateJSONDocument();
		m_cachedDoc["direction"].SetString(szMoveDir);

		gMessageHandler.Send(INVALID_RECIPIENT, RECIPIENT_NET, NET_SEND, 0, &m_cachedDoc);
    
		gGameMgr.IncrementTurns();
		gGameMgr.GetPlayer()->SetPosition(toTile);

		m_status = GOAL_COMPLETE;
	}
	else
	{
		m_status = GOAL_FAILED;
	}

    return m_status;
}

void TraverseEdgeGoal::End()
{
}

bool TraverseEdgeGoal::HandleMessage(Message &msg)
{
    return false;
}

void TraverseEdgeGoal::GenerateJSONDocument()
{
    if(!m_cachedDoc.IsObject())
    {
        m_cachedDoc.SetObject();
        
        rapidjson::Value jsonValMessage;
        jsonValMessage.SetString("action");

        rapidjson::Value jsonValType;
        jsonValType.SetString("move");

        rapidjson::Value jsonValDirection;
        jsonValDirection.SetString("up");//Up is default value.

        m_cachedDoc.AddMember("message", jsonValMessage, m_cachedDoc.GetAllocator());
        m_cachedDoc.AddMember("type", jsonValType, m_cachedDoc.GetAllocator());
        m_cachedDoc.AddMember("direction", jsonValDirection, m_cachedDoc.GetAllocator());
    }
}