#include "FuzzyModule.h"

FuzzyModule::~FuzzyModule()
{
	for(FzRuleVec::iterator iter = m_fzRules.begin();
		iter != m_fzRules.end();++iter)
	{
		SAFE_DELETE((*iter));
	}

    for(FzVarMap::iterator iter = m_fzVariables.begin();
        iter != m_fzVariables.end();++iter)
    {
        SAFE_DELETE(iter->second);
    }
}

FuzzyVariable &FuzzyModule::CreateFLV(int ID)
{  
    FuzzyVariable *fzVar = new FuzzyVariable();
    m_fzVariables.insert(std::pair<int, FuzzyVariable*>(ID, fzVar));

    return *fzVar;
}

void FuzzyModule::AddRule(FuzzyTerm &antecedent, FuzzyTerm &consequence)
{
	m_fzRules.push_back(new FuzzyRule(antecedent, consequence));
}
