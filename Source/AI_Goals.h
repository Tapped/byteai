#ifndef BYTEAI_AI_GOALS_H_
#define BYTEAI_AI_GOALS_H_

#include <stack>
#include "Recipient.h"

enum Goal_Status
{
    GOAL_FAILED,
    GOAL_COMPLETE,
    GOAL_ACTIVE,
    GOAL_INACTIVE,
};

/**
 * Abstract class for a AI goal.
 */
class BaseGoal : public Recipient
{
public:
    BaseGoal(int recpID) : Recipient(recpID), m_status(GOAL_INACTIVE) {}

    virtual ~BaseGoal() {}

    /**
     * Adds a goal to a composite goal. 
     * If this function is called on a atomic goal, it will cause an assertion.
     */
    virtual void AddGoal(BaseGoal *goal) {}

    /**
     * Function called before a goal is processed.
     */
    virtual void Activate() {m_status = GOAL_ACTIVE;}

    /**
     * Function used to process an goal.
     */
    virtual int Process() {return GOAL_FAILED;}

    /**
     * Function used to clean up an goal.
     */
    virtual void End() {}

    /**
     * Handle local messages.
     */
    virtual bool HandleMessage(Message &msg) {return false;}

    /**
     * Is goal active?
     */
    bool IsActive() const {return m_status == GOAL_ACTIVE;}

    /**
     * Activate goal if inactive.
     * It will also activate the goal if the status is ready or failed.
     */
    void ActivateIfInactive() {if(m_status != GOAL_ACTIVE)Activate();}

    /**
     * Returns true if goal is complete.
     */
    bool IsComplete() const {return m_status == GOAL_COMPLETE;}

    /**
     * Returns true if goal has failed.
     */
    bool HasFailed() const {return m_status == GOAL_FAILED;}

    /**
     * Returns the status of the goal.
     */
    int GetStatus() const {return m_status;}
protected:
    int m_status;//!< The status of the goal.
};

/**
 * Atomic goal consist of one goal, which can be processed.
 */
class AtomicGoal : public BaseGoal
{
public:
    AtomicGoal(int recpID=INVALID_RECIPIENT) : BaseGoal(recpID) {}

    virtual ~AtomicGoal() {}

    /**
     * This function is not implemented an will cause an assertion.
     */
    void AddGoal(BaseGoal *goal);

    /**
     * Function called before a goal is processed.
     */
    virtual void Activate() {}

    /**
     * Function used to process an goal.
     */
    virtual int Process() {return GOAL_FAILED;}

    /**
     * Function used to clean up an goal.
     */
    virtual void End() {}

    /**
     * Handle local messages.
     */
    virtual bool HandleMessage(Message &msg) {return false;}
};

/**
 * Composite goal consist of more than one goal.
 */
class CompositeGoal : public BaseGoal
{
public:
    CompositeGoal(int recpID=INVALID_RECIPIENT) : BaseGoal(recpID) {}

    virtual ~CompositeGoal();

    /**
     * Adds a goal to a composite goal. 
     */
    virtual void AddGoal(BaseGoal *goal);

    /**
     * Function called before a goal is processed.
     */
    virtual void Activate() {}

    /**
     * Function used to process goal.
     */
    virtual int Process() {return GOAL_FAILED;}

    /**
     * Function used to process the subgoals
     */
    virtual int ProcessSubgoals();

    /**
     * Function used to clean up an goal.
     */
    virtual void End() {}

    /**
     * Handle local messages.
     */
    virtual bool HandleMessage(Message &msg);

    void Clear();
protected:
    std::stack<BaseGoal*> m_stackOfGoals;
};


#endif