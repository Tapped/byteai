#ifndef BYTEAI_PRIORITY_QUEUE_H_
#define BYTEAI_PRIORITY_QUEUE_H_

#include <vector>
#include "SharedDefs.h"

/**
 * Priority queue based on an index of keys.
 * It prioritize the lowest value.
 */
template<class T>
class IndexedPriorityQLow
{
public:
    IndexedPriorityQLow(std::vector<T> &keys, int maxSize) : m_keys(keys),
                                                      m_maxSize(maxSize),
                                                      m_size(0),
                                                      m_heap(maxSize+1),
                                                      m_heapHandler(maxSize+1) {}

    /**
     * Returns true if queue is empty.
     */
    bool Empty() const
    {
        return m_size == 0;
    }

    /**
     * Insert an element.
     */
    void Insert(int index)
    {
        Assert(m_size + 1 <= m_maxSize && 
                "PriorityQueue::Insert() not enough memory!");

        ++m_size;

        m_heap[m_size] = index;
        m_heapHandler[index] = m_size;

        ReorderUpwards(m_size);
    }

    /**
     * Returns the element with the lowest value.
     */
    int Pop()
    {
        Swap(1, m_size);
        ReorderDownwards(1, m_size - 1);

        return m_heap[m_size--];
    }

    /**
     * If the key have changed, update the priority queue accordingly.
     */
    void ChangePriority(int index)
    {
        ReorderUpwards(m_heapHandler[index]);
    }
private:
    std::vector<T> &m_keys;
    std::vector<int> m_heap;
    std::vector<int> m_heapHandler;

    int m_size;
    int m_maxSize;

    /**
     * Swaps the keys and handler.
     */
    void Swap(int a, int b)
    {
        int tmp = m_heap[a];
        m_heap[a] = m_heap[b];
        m_heap[b] = tmp;

        m_heapHandler[m_heap[a]] = a;
        m_heapHandler[m_heap[b]] = b;
    }

    void ReorderUpwards(int n)
    {
        int halfN = 0;
        //Use bitshift to do n / 2 as fast as possible.
        while(n > 1 && m_keys[m_heap[(halfN = n >> 1)]] > m_keys[m_heap[n]])
        {
            Swap(halfN, n);

            n = halfN;
        }
    }

    void ReorderDownwards(int n, int heapSize)
    {
        //Use bitshift to do n * 2 as fast as possible.
        int doubleN = 0;
        while((doubleN = n << 1) <= heapSize)
        {
            if(doubleN < heapSize && m_keys[m_heap[doubleN]] > m_keys[m_heap[doubleN+1]])
            {
                ++doubleN;
            }

            if(m_keys[m_heap[n]] > m_keys[m_heap[doubleN]])
            {
                Swap(doubleN, n);

                n = doubleN;
            }
            else
            {
                return;
            }
        }
    }
};

#endif