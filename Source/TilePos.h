#ifndef BYTEAI_TILE_POS_H_
#define BYTEAI_TILE_POS_H_

#include "MathUtil.h"
#include "SharedDefs.h"

#include <sstream>

#define max3(a, b, c) (a > b ? (a > c ? a : c) : (b > c ? b : c)) 

#define UP "up"
#define DOWN "down"
#define RIGHT_UP "right-up"
#define RIGHT_DOWN "right-down"
#define LEFT_UP "left-up"
#define LEFT_DOWN "left-down"
#define INVALID_DIRECTION NULL
/**
 * TilePos represents a position in the game.
 */
struct TilePos
{
    int j;
    int k;

    TilePos() : j(0), k(0){}
    TilePos(int J, int K) : j(J), k(K) {}

    TilePos operator-(const TilePos &a) const
    {
        return TilePos(j - a.j, k - a.k);
    }

    TilePos operator+(const TilePos &a) const
    {
        return TilePos(j + a.j, k + a.k);
    }
    
	TilePos &operator +=(const TilePos &a)
	{
		j += a.j;
		k += a.k;
		return *this;
	}

    bool operator==(const TilePos &a) const
    {
        return (j == a.j && k == a.k);
    }

	bool operator !=(const TilePos &a) const
	{
		return (j != a.j || k != a.k);
	}

	std::string GetString() const
	{
		std::stringstream ss;
		ss << j << "," << k;
		return ss.str();
	}

};

inline int Distance(const TilePos &a, const TilePos &b)
{
    //This function is not well enough tested, but it works for small distance after-all.
    TilePos toB = b - a;

	if(toB.j > 0 && toB.k > 0)
	{
		return max(std::abs(toB.j), std::abs(toB.k));
	}
	else
	{
		return abs(toB.j) + abs(toB.k);
	}
}

inline bool IsZero(const TilePos &a)
{
	return a.j == 0 && a.k == 0;
}

inline const char* GetDirection(const TilePos &a, const TilePos &b)
{
    TilePos toB = b - a;

    if(toB.j == 0)
    {
        if(toB.k > 0)
        {
            return RIGHT_DOWN;
        }
        else
        {
            return LEFT_UP;
        }
    }
    else if(toB.k == 0)
    {
        if(toB.j > 0)
        {
            return LEFT_DOWN;
        }
        else
        {
            return RIGHT_UP;
        }
    }
    else if(toB.j > 0 && toB.j == toB.k)
    {
        return DOWN;
    }
    else if (toB.j < 0 && toB.j == toB.k)
    {
        return UP;
    }else {
		return INVALID_DIRECTION;
	}
}

inline int GetDirectionAsInt(const TilePos &a, const TilePos &b)
{
	TilePos toB = b - a;

    if(toB.j == 0)
    {
        if(toB.k > 0)
        {
            return 3;
        }
        else
        {
            return 2;
        }
    }
    else if(toB.k == 0)
    {
        if(toB.j > 0)
        {
            return 5;
        }
        else
        {
            return 4;
        }
    }
    else if(toB.j > 0 && toB.j == toB.k)
    {
        return 1;
    }
    else if (toB.j < 0 && toB.j == toB.k)
    {
        return 0;
    }else {
		return -1;
	}
}

#endif