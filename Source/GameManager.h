#ifndef BYTEAI_GAME_MANAGER_H_
#define BYTEAI_GAME_MANAGER_H_

#include "NetworkManager.h"
#include "Map.h"
#include "Player.h"
#include "PlayerManager.h"
#include "AI_Brain.h"

/**
 * Game manager that manages all logic.
 * It is used as a singleton
 */
class GameManager
{
public:
	~GameManager();

	static GameManager &GetInstance()
	{
		static GameManager instance;
		return instance;
	}

	/**
	 * Initalize the game components.
	 */
	bool Init();

	/**
	 * Update AI and network systems.
	 */
	void Update();

	/**
	 * Close connection, and free any heap memory. 
	 */
	void Shutdown();

	/**
	 * Returns true while game is still running.
	 */
	bool IsGameOn() const;

    Player *GetPlayer()
    {
        return m_playerMgr.GetOurPlayer();
    }

    bool HasPlayer() const
    {
        return m_playerMgr.HasPlayer();
    }

    const Map *GetMap() const
    {
        return &m_map;
    }

    /**
     * This is set if it is our turn.
     */
    void SetOurTurn(bool turn)
    {
        m_bOurTurn = turn;
		m_turnsUsed = 0;
    }

	/** 
	 * Increments turn so that the AI knows how many turns it have left.
	 */
    void IncrementTurns()
    {
        ++m_turnsUsed;
        if(m_turnsUsed > 2)
        {
            m_bOurTurn = false;
            m_turnsUsed = 0;
        }
    }

	/**
	 * Returns the relative turn from last gamestate package.
	 * In other words a value = [0, 2].
	 */
	int GetTurn()
	{
		return m_turnsUsed;
	}

    /** 
     * Is it our turn?
     */
    bool OurTurn() const
    {
        return m_bOurTurn;
    }

	/**
	 * Returns true if the server has sent the gamestart state.
	 */
    bool HasGameStarted() const
    {
        return m_bGameHasStarted;
    }

	/**
	 * Returns the player manager.
	 */
	const PlayerManager *GetPlayerMgr() const 
	{
		return &m_playerMgr;
	}

	/**
	 * True if the server has sent the gamestart state.
	 */
    void SetGameStarted(bool gameStarted)
    {
        m_bGameHasStarted = gameStarted;
    }
private:
    GameManager() : m_bGameOn(true), m_bOurTurn(false), m_turnsUsed(0) {}
	GameManager(const GameManager&) {}
	GameManager &operator =(const GameManager&) {}

    AI_Brain m_aiBrain;//!< Manages decision that the AI do or should do.
    PlayerManager m_playerMgr;//!< Player manager.
	Map m_map;//!< Map information is stored here.
	NetworkManager m_netManager;//!< The network manager.
    int m_turnsUsed;//!< The number of turns that the AI have used.
	bool m_bGameOn;//!< Is game on?
    bool m_bOurTurn;//<! Is it our turn?
    bool m_bGameHasStarted;//<! Has the game started yet?
};

#define gGameMgr GameManager::GetInstance()

#endif