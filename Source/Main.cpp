#include "GameManager.h"
#include "SharedDefs.h"
#include <iostream>
#include "TilePos.h"

#ifdef _WIN32
#pragma comment(lib, "Ws2_32.lib")
#endif

int main(int argc, char **argv)
{
    srand((unsigned int)time(0));

	std::cout << "Starting the genius ByteAI :)" << std::endl;

    do
    {
        std::cout << std::endl;
	    if(gGameMgr.Init())
	    {
		    while(gGameMgr.IsGameOn())
		    {
			    gGameMgr.Update();
		    }
        }
        
        std::cout << "AI was terminated, do you want to restart? y/n: ";
    }while(std::cin.get() == 'y');

	return 0;
}

