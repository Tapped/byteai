#ifndef BYTEAI_ATTACK_MORTAR_GOAL_H_
#define BYTEAI_ATTACK_MORTAR_GOAL_H_

#include "AI_Goals.h"
#include "Weapons.h"
#include "Player.h"
#include <rapidjson/document.h>

/**
 * Atomic goal, which fires a mortar.
 */
class AttackMortarGoal : public AtomicGoal
{
public:
	AttackMortarGoal(const Player *target, const Player *attacker) : m_target(target), m_attacker(attacker) {}

	void Activate();

	int Process();

	void End();
private:
	static void PrecacheJSONDocument();
	static rapidjson::Document m_jsonDoc;

	const Player *m_target;
	const Player *m_attacker;
};

#endif