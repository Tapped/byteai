#include "MoveToGoal.h"
#include "GameManager.h"
#include "TraverseEdgeGoal.h"

void MoveToGoal::Activate()
{
    Clear();

    m_path.clear();
    gGameMgr.GetMap()->FindShortestPath(m_sourcePosition, m_targetPosition, m_path);
    
    if(!m_path.empty())
    {
        m_oldNode = *(m_path.begin());
        m_path.pop_front();
		m_status = GOAL_ACTIVE;
    }
	else
	{
		m_status = GOAL_FAILED;
	}
}

int MoveToGoal::Process()
{
    ActivateIfInactive();

	if(m_status == GOAL_FAILED)
	{
		return m_status;
	}

    m_status = ProcessSubgoals();

    //If the subgoals are complete but we still got more edges to traverse.
    if(m_status == GOAL_COMPLETE && !m_path.empty())
    {  
        int from = m_oldNode;
        int to = *(m_path.begin());
		m_path.pop_front();
		
		m_oldNode = to;

		if(!gGameMgr.GetMap()->IsInaccesibleNode(to))
		{
			AddGoal(new TraverseEdgeGoal(from, to));

			m_status = GOAL_ACTIVE;
		}
		else
		{
			m_status = GOAL_FAILED;
		}
    }

    return m_status;
}

void MoveToGoal::End()
{
    Clear();
}

bool MoveToGoal::HandleMessage(Message &msg)
{
    bool isMsgRead = CompositeGoal::HandleMessage(msg);
    if(!isMsgRead)
    {

    }

    return isMsgRead;
}

void MoveToGoal::SetTargetPos(const TilePos &targetPos)
{
    m_targetPosition = targetPos;
}

void MoveToGoal::SetSourcePos(const TilePos &sourcePos)
{
    m_sourcePosition = sourcePos;
}

