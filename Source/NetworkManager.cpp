#include <iostream>
#include <cstring>

#include "NetworkManager.h"
#include "GameManager.h"
#include "MessageHandler.h"
#include "SharedDefs.h"
#include <rapidjson/stringbuffer.h>
#include <rapidjson/writer.h>

//We will use the network buffer to temporary store map data, for optimization.
//This is the offset into that buffer.
#define MAP_DATA_OFFSET 2048

const char szHandShake[] = "{\"message\":\"connect\","
    "\"revision\":1,"
    "\"name\":\"Byte\"}\n";

bool NetworkManager::Connect()
{
	addrinfo *pResult = NULL, *pCurrent = NULL, hints;
	int iResult;
#ifdef _WIN32
	//Initialize WinSocket
	iResult = WSAStartup(MAKEWORD(2, 2), &m_wsaData);
	if(iResult != 0)
	{
#ifdef DEBUG_NETWORK
		std::cout << "WSAStartup failed: " << iResult << std::endl;
#endif
		return false;
	}
#endif

	ZeroMemory(&hints, sizeof(hints));
	hints.ai_family = AF_UNSPEC;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_protocol = IPPROTO_TCP;

	//Get address info
	iResult = getaddrinfo("127.0.0.1", DEFAULT_PORT, &hints, &pResult);
	if(iResult != 0)
	{
#ifdef DEBUG_NETWORK
		std::cout << "getaddrinfo failed: " << iResult << std::endl;
#endif
		WSACleanup();
		return false;
	}

	//Create socket for listening
	m_clientSocket = socket(pResult->ai_family, pResult->ai_socktype, pResult->ai_protocol);
	if(m_clientSocket == INVALID_SOCKET)
	{
#ifdef DEBUG_NETWORK
		std::cout << "Error when executing socket(): " << WSAGetLastError() << std::endl;
#endif
		freeaddrinfo(pResult);
		WSACleanup();
		return false;
	}

	//Bind the socket to default address
	iResult = connect(m_clientSocket, pResult->ai_addr, (int)pResult->ai_addrlen);
	if(iResult == SOCKET_ERROR)
	{
#ifdef DEBUG_NETWORK
		std::cout << "Error when calling connect: " << WSAGetLastError() << std::endl;
#else
		std::cout << "Unable to connect to server!" << std::endl;
#endif
		freeaddrinfo(pResult);
		closesocket(m_clientSocket);
		WSACleanup();
		return false;
	}

	freeaddrinfo(pResult);
	
	if (m_clientSocket == INVALID_SOCKET) 
	{
		std::cout << "Unable to connect to server!" << std::endl;
		WSACleanup();
		return false;
	}
    
    send(m_clientSocket, szHandShake, sizeof(szHandShake), 0);

#ifdef _WIN32
	m_netEvent = WSACreateEvent();

    if(WSAEventSelect(m_clientSocket, m_netEvent, FD_READ | FD_WRITE))
	{
#ifdef DEBUG_NETWORK
		std::cout << "Error when calling WSAEventSelect: " << WSAGetLastError() << std::endl;
#endif
		WSACleanup();
		return false;
	}
#endif

    m_bCanSend = false;
    m_currentInPos = 0;
    m_startOfData = 0;

    gMessageHandler.AddRecipient(this);

	return true;
}

void NetworkManager::Update()
{
#ifdef _WIN32
	WSANETWORKEVENTS eventStatus;

	if(WSAEnumNetworkEvents(m_clientSocket, m_netEvent, &eventStatus))
	{
#ifdef DEBUG_NETWORK
		std::cout << "Error when calling WSAEnumNetworkEvents: " << WSAGetLastError() << std::endl;
#endif
		gGameMgr.Shutdown();
	}

	//Check if we got a packet to process
	if(eventStatus.lNetworkEvents & FD_READ)
	{
		HandleReceive();
	}
    
    if(eventStatus.lNetworkEvents & FD_WRITE)
    {
        m_bCanSend = true;
    }
#endif
}

void NetworkManager::HandleReceive()
{
    int iNumBytesReceived = recv(m_clientSocket, &m_netBuffer[m_currentInPos], NETWORK_BUFFER_SIZE - m_currentInPos, 0);

    //Close socket when we received 0 bytes.
    if(iNumBytesReceived == 0)
    {
        gGameMgr.Shutdown();
    }

    char *currentPos = &m_netBuffer[m_startOfData];
    char *pNewLine = strchr(currentPos, '\n');
    while(pNewLine)
    {
        *pNewLine = '\0';

        rapidjson::Document jsonDoc;
        jsonDoc.ParseInsitu<0>(currentPos);
	    if(jsonDoc.HasParseError() || !jsonDoc.IsObject())
        {
            std::cout << "Failed to parse JSON document received from server!" << std::endl;
        }
        else
        {
            if(jsonDoc.HasMember("message"))
            {
	            std::string msgType = jsonDoc["message"].GetString();
	            if(msgType.compare("gamestate") == 0)
	            {
		            OnGameState(jsonDoc);
	            }
#if !defined(DEBUG_NETWORK)
            }
#endif
#ifdef DEBUG_NETWORK
	            else if(msgType.compare("connect") == 0)
	            {
		            OnConnect(jsonDoc);
	            }
        	    else
	            {
		            std::cout << "Unknown message type!" << std::endl;
	            }
            }
	    
            if(jsonDoc.HasMember("error"))
	        {
		        std::cout << "Server sent an error: " << jsonDoc["error"].GetString() << std::endl;
	        }
#endif
        }

        currentPos = pNewLine+1;
        pNewLine = strchr(pNewLine+1, '\n');
    }

    int readSize = currentPos - &m_netBuffer[m_currentInPos];
    //We have read everything...
    if(readSize == iNumBytesReceived)
    {
        //Be sure to clear the net buffer.
        memset(m_netBuffer, '\0', m_currentInPos + iNumBytesReceived);
        m_currentInPos = 0;
        m_startOfData = 0;
    }
    //We have more data to read
    else
    {
        m_currentInPos += iNumBytesReceived;
        m_startOfData = currentPos - m_netBuffer;
    }
}

#ifdef DEBUG_NETWORK
void NetworkManager::OnConnect(rapidjson::Document &doc)
{
	std::cout << "Received connect message." << std::endl;
	if(!doc["status"].GetBool())
	{
		std::cout << "Server refused YOU to join the game!" << std::endl;
	}
}
#endif

void NetworkManager::OnGameState(rapidjson::Document &doc)
{
#ifdef DEBUG_NETWORK
	std::cout << "Received gamestate message." << std::endl;
#endif

	int turn = doc["turn"].GetInt();
    bool gameStart = turn <= 0;

	ExtraData_Map extraDataMap;
	const rapidjson::Value &jsMap = doc["map"];
	const rapidjson::Value &jsDataMap = jsMap["data"];

	extraDataMap.jLength = jsMap["j-length"].GetInt();
	extraDataMap.kLength = jsMap["k-length"].GetInt();
	extraDataMap.Data = &m_netBuffer[MAP_DATA_OFFSET];

	Assert(jsDataMap.IsArray());
	for(rapidjson::SizeType j = 0;j < jsDataMap.Size();j++)
	{
		const rapidjson::Value &jsData = jsDataMap[j];
		Assert(jsData.IsArray());
		uint kSize = jsData.Size();

		for(rapidjson::SizeType k = 0;k < kSize;k++)
		{
			m_netBuffer[MAP_DATA_OFFSET + (j*kSize) + k] = *jsData[k].GetString();
		}
	}

    ExtraData_Players extraDataPlayers;
    const rapidjson::Value &jsPlayers = doc["players"];
    Assert(jsPlayers.IsArray());
    extraDataPlayers.numPlayers = jsPlayers.Size(); 
    for(rapidjson::SizeType i = 0;i < jsPlayers.Size();i++)
    {
        const rapidjson::Value &jsPlayer = jsPlayers[i];
        PlayerData *pCurrentPlayer = &extraDataPlayers.players[i];
        pCurrentPlayer->name = jsPlayer["name"].GetString();

        //We do not receive more than player name with gamestart package.
        if(!gameStart)
        {
            if(i == 0)
            {
                if(pCurrentPlayer->name == AI_NAME)
                {
                    //It is our turn.
                    gGameMgr.SetOurTurn(true);
                }
                else
                {
                    //It is not our turn.
                    gGameMgr.SetOurTurn(false);
                }
            }

            const rapidjson::Value &jsPrimWeapon = jsPlayer["primary-weapon"];
            pCurrentPlayer->primWeapon = jsPrimWeapon["name"].GetString();
            pCurrentPlayer->levelPrimWeapon = jsPrimWeapon["level"].GetInt();

            const rapidjson::Value &jsSecWeapon = jsPlayer["secondary-weapon"];
            pCurrentPlayer->secondWeapon = jsSecWeapon["name"].GetString();
            pCurrentPlayer->levelSecondWeapon = jsSecWeapon["level"].GetInt();

            pCurrentPlayer->score = jsPlayer["score"].GetInt();
            pCurrentPlayer->health = jsPlayer["health"].GetInt();

            std::string posStr = jsPlayer["position"].GetString();
            size_t commaPos = posStr.find(',');
            pCurrentPlayer->position.j = atoi(posStr.c_str());
            pCurrentPlayer->position.k = atoi(posStr.c_str() + commaPos + 1);
        }
    }

    gMessageHandler.Send(RECIPIENT_NET, RECIPIENT_PLAYERMGR, PLAYERS_UPDATE, 0, &extraDataPlayers);
    gMessageHandler.Send(RECIPIENT_NET, RECIPIENT_MAP, turn > 0 ? MAP_UPDATE : MAP_INIT, 0, &extraDataMap);

    //Choose weapon and send loadout.
    if(gameStart)
    {
        const Map *pMap = gGameMgr.GetMap();
        Player *pPlayer = gGameMgr.GetPlayer();

        pPlayer->ChooseWeapons(pMap->GetSize(), pMap->AmountOfATileType(TILE_SCRAP), 
        pMap->AmountOfATileType(TILE_EXPLOSIUM), pMap->AmountOfATileType(TILE_RUBIDIUM));

        gGameMgr.SetGameStarted(true);
    }
}

bool NetworkManager::HandleMessage(Message &msg)
{
	Assert(msg.ExtraData && "NetworkManager::HandleMessage() expects ExtraData in message.");
	rapidjson::Document *pDoc = reinterpret_cast<rapidjson::Document*>(msg.ExtraData);
	switch(msg.Msg)
	{
	case NET_SEND:
		Send(*pDoc);
        return true;
		break;
	default:
		std::cout << "Invalid message received by internal message handler in NetworkManager!" << std::endl;
		break;
	}

    return false;
}

void NetworkManager::Send(rapidjson::Document &doc)
{
	rapidjson::StringBuffer buffer;
	rapidjson::Writer<rapidjson::StringBuffer> writer(buffer);
	doc.Accept(writer);
    
    int bufferSize = buffer.Size();
    int numBytesSendt = 0;

    memcpy(&m_outBuffer[1], buffer.GetString(), bufferSize);

    //Hack to make the message work. Very weird...
    m_outBuffer[0] = '\n';
    m_outBuffer[bufferSize+1] = '\n';

	//std::cout << "Turn: " << gGameMgr.GetTurn();
    //std::cout << m_outBuffer << std::endl;

    if(m_bCanSend)
    {
        if((numBytesSendt = send(m_clientSocket, m_outBuffer, bufferSize+2, 0) == SOCKET_ERROR))
	    {
            if(WSAGetLastError() == WSAEWOULDBLOCK)
            {
                //The internal socket buffer is full, so wait for the socket to be ready again.
                WSAWaitForMultipleEvents(1, &m_netEvent, FALSE, WSA_INFINITE, FALSE);
            }
            else
            {
    #ifdef _DEBUG
		        std::cout << "Could not send message to server! Disconnecting..." << std::endl;
    #endif
		        //Shuttingdown
		        gGameMgr.Shutdown();
            }
	    }
    }

    memset(m_outBuffer, 0, bufferSize+1);
}

void NetworkManager::Close()
{
    closesocket(m_clientSocket);

#ifdef _WIN32
	WSACloseEvent(m_netEvent);
	WSACleanup();
#endif
}