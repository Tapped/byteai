#include "GameManager.h"
#include "NetworkManager.h"
#include "Timer.h"
#include "MessageHandler.h"

GameManager::~GameManager()
{
    if(m_bGameOn)
    {
        Shutdown();
    }
}

bool GameManager::Init()
{
	if(!m_netManager.Connect())
	{
		return false;
	}

	if(!Timer::Init())
	{
		return false;
	}

    m_playerMgr.Init();
	m_map.Init();
    
    m_aiBrain.Init();

	return true;
}

void GameManager::Update()
{
	gMessageHandler.Update();
	m_netManager.Update();

    if(m_bOurTurn)
    {
        m_aiBrain.Update();
    }

#ifdef _DEBUG
    //Save some CPU processing...
    Sleep(100);
#endif
}

void GameManager::Shutdown()
{
	m_bGameOn = false;

	m_netManager.Close();
	m_map.Release();
}

bool GameManager::IsGameOn() const
{
	return m_bGameOn;
}