#include "AttackMortarGoal.h"
#include "MessageHandler.h"
#include "GameManager.h"

rapidjson::Document AttackMortarGoal::m_jsonDoc;

void AttackMortarGoal::Activate()
{
	m_status = GOAL_ACTIVE;
}

int AttackMortarGoal::Process()
{
	ActivateIfInactive();

	PrecacheJSONDocument();

	TilePos toTarget = m_target->GetPosition() - m_attacker->GetPosition();
	std::string targetStr = toTarget.GetString();
	m_jsonDoc["coordinates"].SetString(targetStr.c_str());

	gMessageHandler.Send(INVALID_RECIPIENT, RECIPIENT_NET, NET_SEND, 0, &m_jsonDoc);

	//Attack goal would end the turn, however the damage is multiplied with the amount of turns left.
	gGameMgr.SetOurTurn(false);

	m_status = GOAL_COMPLETE;

	return m_status;
}

void AttackMortarGoal::End()
{
}

void AttackMortarGoal::PrecacheJSONDocument()
{
	if(!m_jsonDoc.IsObject())
	{
		rapidjson::Value message;
		rapidjson::Value type;
		rapidjson::Value coordinates;

		coordinates.SetString("1,0");
		message.SetString("action");
		type.SetString("mortar");
		
		m_jsonDoc.SetObject();
		m_jsonDoc.AddMember("message", message, m_jsonDoc.GetAllocator());
		m_jsonDoc.AddMember("type", type, m_jsonDoc.GetAllocator());
		m_jsonDoc.AddMember("coordinates", coordinates, m_jsonDoc.GetAllocator());
	}
}