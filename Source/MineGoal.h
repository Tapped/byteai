#ifndef BYTEAI_MINE_GOAL_H_
#define BYTEAI_MINE_GOAL_H_

#include "AI_Goals.h"
#include "Player.h"
#include <rapidjson/document.h>


/**
 * Attacks a target.
 * It will choose the best weapon for the situation.
 */
class MineGoal : public CompositeGoal
{
public:
	MineGoal(const Player *attacker, int targetType) : m_attacker(attacker), m_targetType(targetType) {}

	/**
	 * Activates the goal, and choose the best weapon.
	 */
	void Activate();

	/**
	 * Process the goal, updating what's needs to be updated.
	 */
	int Process();

	/**
	 * End is called when this goal terminates.
	 */
	void End();

	bool HandleMessage(Message &msg);
private:
	static void PrecacheJSONDocument();
    static rapidjson::Document m_jsonDoc;

	const Player *m_attacker;//!< The attacker
	int m_targetType;//!< Target tile type
};
#endif