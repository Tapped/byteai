#ifndef BYTEAI_TRAVERSE_EDGE_GOAL_H_
#define BYTEAI_TRAVERSE_EDGE_GOAL_H_

#include "AI_Goals.h"
#include "TilePos.h"
#include <rapidjson/document.h>

class TraverseEdgeGoal : public AtomicGoal
{
public:
    TraverseEdgeGoal(int from, int to) : m_from(from), m_to(to) {}
    
    /**
     * Function called before an goal is processed.
     */
    void Activate();

    /**
     * Function used to process an goal.
     */
    int Process();

    /**
     * Function used to clean up an goal.
     */
    void End();

    /**
     * Handle local messages.
     */
    bool HandleMessage(Message &msg);
	
private:
   

    int m_from;
    int m_to;

    static void GenerateJSONDocument();//!< This function generate the json document the first time.
    static rapidjson::Document m_cachedDoc;//!< We store a template document for faster json generation. 
};

#endif