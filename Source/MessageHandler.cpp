#include "MessageHandler.h"
#include "Timer.h"
#include "SharedDefs.h"
#include <cassert>
#include <iostream>

void MessageHandler::Send(uint from, uint to, uint message, uint delay, void *extraData)
{
	Message msg(from, to, message, extraData);

	if(delay > 0)
	{
		msg.DispatchTime = Timer::GetGlobalMillisecUINT() + delay;
		m_messages.insert(msg);
	}
	else
	{
        if(to == RECIPIENT_ALL)
        {
            //Send message to everyone except the sender.
            for(ReciepientsIter iter = m_reciepients.begin();
                iter != m_reciepients.end();++iter)
            {
                if(iter->second->GetID() != from)
                {
                    Discharge(iter->second, msg);
                }
            }
        }
        else
        {
		    if(!Discharge(GetRecipient(to), msg))
            {
                std::cout << "The recipient("<< to << ") could not handle message(" << msg.Msg << ")\n";
            }
        }
	}
}

void MessageHandler::Update()
{
	Message msg;
	uint currentTime = Timer::GetGlobalMillisecUINT();

    while(!m_messages.empty() && m_messages.begin()->DispatchTime < currentTime && m_messages.begin()->DispatchTime > 0)
	{
		msg = *m_messages.begin();

        if(msg.To == RECIPIENT_ALL)
        {
            //Send message to everyone except the sender.
            for(ReciepientsIter iter = m_reciepients.begin();
                iter != m_reciepients.end();++iter)
            {
                if(iter->second->GetID() != msg.From)
                {
                    Discharge(iter->second, msg);
                }
            }
        }
        else
        {
		    Discharge(GetRecipient(msg.To), msg);
        }

		m_messages.erase(m_messages.begin());
    }
}

void MessageHandler::AddRecipient(Recipient *recipient)
{
	Assert(recipient && recipient->m_id != INVALID_RECIPIENT 
        && "MessageHandler::AddRecipient() Invalid recipient.");
	m_reciepients.insert(std::pair<uint, Recipient*>(recipient->m_id, recipient));
}

void MessageHandler::RemoveRecipient(Recipient *recipient)
{
	Assert(recipient && "MessageHandler::RemoveRecipient() recipient can't be a NULL pointer.");
#ifdef _DEBUG
	if(recipient->m_id < 0)
	{
		std::cout << "Invalid recipient ID, at line: " << __LINE__ << "File: " << __FILE__ << std::endl;
	}
#endif
	
	ReciepientsIter element = m_reciepients.find(recipient->m_id);
	if(element != m_reciepients.end())
	{
		m_reciepients.erase(element);
	}
}

bool MessageHandler::Discharge(Recipient *recipient, Message &msg)
{
	Assert(recipient && "MessageHandler::Discharge() could not find recipient!");
    if(recipient)
    {
	    return recipient->HandleMessage(msg);
    }
    else
    {
        std::cout << "Could not find recipient(" << recipient->GetID() << ")\n";
        return false;
    }
}

Recipient *MessageHandler::GetRecipient(uint id) const
{
    Assert(id != INVALID_RECIPIENT && "MessageHandler::GetRecipient() invalid recipient.");

	ReciepientsIter iter = m_reciepients.find(id);
	if(iter != m_reciepients.end())
	{
		return iter->second;
	}
	
	return 0;
}