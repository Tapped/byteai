#ifndef BYTEAI_WEAPONS_H_
#define BYTEAI_WEAPONS_H_

#include "FuzzyModule.h"
#include "Recipient.h"
#include "WeaponInfo.h"

enum WeaponTypes
{
    WPN_UNKOWN,
    WPN_MORTAR,
    WPN_LASER,
    WPN_DROID
};

/**
 * Base class for weapons.
 */
class Weapon
{
public:
	Weapon(int rangeLevelOne, int rangeLevelTwo, int rangeLevelThree, int damageLevelOne, int damageLevelTwo, int damageLevelThree, int aoeDamage, int type) : m_type(type), m_aoeDamage(aoeDamage) 
	{
		m_range[0] = rangeLevelOne; 
		m_range[1] = rangeLevelTwo; 
		m_range[2] = rangeLevelThree;

		m_damage[0] = damageLevelOne;
		m_damage[1] = damageLevelTwo;
		m_damage[2] = damageLevelThree;
	}

    int GetType() const
    {
        return m_type;
    }

    /**
     * @return Weapon name as string.
     */
    virtual const char *GetString() = 0;

	/**
	 * Calculates the desirability to use this weapon.
	 */
	virtual double CalculateDesirability(int distance) const = 0;

	/**
	 * Returns the range of the weapon.
	 */
	int GetRange() const {return m_range[m_level];}

	bool HasAOEDmg() const {return m_aoeDamage != 0;}

	/**
	 * Set the level of the gun.
	 */
	void SetLevel(int level)
	{
		m_level = level;
	}

	/** 
	 * Returns the level.
	 */
	int GetLevel() const
	{
		return m_level;
	}

	/** 
	 * Returns the damage.
	 */
	int GetDamage() const
	{
		m_damage[m_level];
	}
protected:
    FuzzyModule m_fzModule;
    int m_damage[3];//!< Current damage of weapon.
	int m_aoeDamage;//!< Area of effect damage.
    int m_range[3];//!< Current range of weapon.
	int m_level;//!< Weapons can be upgraded and this is the weapon level.
	int m_type;//!< Type of weapon.
};

class WeaponMortar : public Weapon, public Recipient
{
public:
	WeaponMortar() : Recipient(RECIPIENT_MORTAR), Weapon(MORTAR_RANGE_LEVEL_1, MORTAR_RAMGE_LEVEL_2, MORTAR_RANGE_LEVEL_3,  MORTAR_DMG_LEVEL_1, MORTAR_DMG_LEVEL_2, MORTAR_DMG_LEVEL_3, MORTAR_AOE_DMG, WPN_MORTAR) {}

    /**
     * Calculate the desireability for a weapon at start.
     * @param mapSize Size of map.
     * @param numRes Number of resources.
     * @param distance The distance to the nearest resource.
     * @return Desirability for this specific weapon.
     */
    static double CalculateDesirabilityAtStart(int mapSize, int numRes);

	/**
	 * Calculates the desirability to use this weapon.
	 */
	double CalculateDesirability(int distance) const;

    bool HandleMessage(Message &msg){return false;}

    const char *GetString()
    {
        return "mortar";
    }
};

class WeaponLaser : public Weapon, public Recipient
{
public:
	WeaponLaser() : Recipient(RECIPIENT_LASER), Weapon(LASER_RANGE_LEVEL_1, LASER_RANGE_LEVEL_2, LASER_RANGE_LEVEL_3, LASER_DMG_LEVEL_1, LASER_DMG_LEVEl_2, LASER_DMG_LEVEL_3, LASER_AOE_DMG, WPN_LASER) {}

    /**
     * Calculate the desireability for a weapon at start.
     * @param mapSize Size of map.
     * @param numRes Number of resources.
     * @param distance The distance to the nearest resource.
     * @return Desirability for this specific weapon.
     */
    static double CalculateDesirabilityAtStart(int mapSize, int numRes);

	/**
	 * Calculates the desirability to use this weapon.
	 */
	double CalculateDesirability(int distance) const;

    bool HandleMessage(Message &msg){return false;}

    const char *GetString()
    {
        return "laser";
    }
};

class WeaponDroid : public Weapon, public Recipient
{
public:
	WeaponDroid() : Recipient(RECIPIENT_DROID), Weapon(DROID_RANGE_LEVEL_1, DROID_RANGE_LEVEL_2, DROID_RANGE_LEVEL_3, DROID_DMG_LEVEL_1, DROID_DMG_LEVEL_2, DROID_DMG_LEVEL_3, DROID_AOE_DMG, WPN_DROID) {}

    /**
     * Calculate the desireability for a weapon at start.
     * @param mapSize Size of map.
     * @param numRes Number of resources.
     * @param distance The distance to the nearest resource.
     * @return Desirability for this specific weapon.
     */
    static double CalculateDesirabilityAtStart(int mapSize, int numRes);

	/**
	 * Calculates the desirability to use this weapon.
	 */
	double CalculateDesirability(int distance) const;

    bool HandleMessage(Message &msg) {return false;}

    const char *GetString()
    {
        return "droid";
    }
};

inline int GetWpnTypeFromString(std::string &weapon)
{
	if(weapon == "mortar")
	{
		return WPN_MORTAR;
	}
	else if(weapon == "laser")
	{
		return WPN_LASER;
	}
	else if(weapon == "droid")
	{
		return WPN_DROID;
	}

	Assert(false && "Weapon undefined!");
}

#endif