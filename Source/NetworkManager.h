#ifndef BYTEAI_NETWORK_MANAGER_H_
#define BYTEAI_NETWORK_MANAGER_H_

#ifdef _WIN32

#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN
#endif

#include <windows.h>
#include <winsock2.h>
#include <ws2tcpip.h>
#include <iphlpapi.h>
#elif defined __unix__
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h> 

//A little hack for unix systems.
#define closesocket(p) close(p)

#endif

#include <map>
#include <vector>
#include <rapidjson\document.h>

#include "Recipient.h"
#include "SharedDefs.h"

/**
 * Manages the network traffic.
 */
class NetworkManager : public Recipient
{
public:
	NetworkManager() : Recipient(RECIPIENT_NET) { }

	/**
	 * Connects to the server
	 */
	bool Connect();

	/**
	 * Checks for new packages. 
	 */
	void Update();

	/**
	 * Closes the connection to server.
	 */
	void Close();

	/**
	 * Handle all the internal messages.
	 */
	bool HandleMessage(Message &msg);
private:
	/**
	 * Handles receives, when the socket is ready for that.
	 */
	void HandleReceive();
 
    /**
     * Send data that lies on the stack.
     */
    void HandleSend();

	/**
	 * Send message to server.
	 */
	void Send(rapidjson::Document &doc);

	/**
	 * Called when a game state message is received.
	 */
	void OnGameState(rapidjson::Document &doc);

#ifdef DEBUG_NETWORK
	/**
	 * Called when a connect message is received.
	 */
	void OnConnect(rapidjson::Document &doc);
#endif

	SOCKET m_clientSocket;
	WSAData m_wsaData;
	WSAEVENT m_netEvent;
    
    int m_currentInPos;
    int m_startOfData;
	char m_netBuffer[NETWORK_BUFFER_SIZE];
    char m_outBuffer[NETWORK_BUFFER_SIZE];
    bool m_bCanSend;
};

#endif