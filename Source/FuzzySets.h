#ifndef BYTEAI_FUZZY_SET_H_
#define BYTEAI_FUZZY_SET_H_

#include "MathUtil.h"
#include <iostream>

/**
 * Base class for all fuzzy sets.
 */
class FuzzySet
{
protected:
	double m_DOM;//!< Stores the degree of membership in this set.

	/**
	 * Stores the position of the maximum value for the membership function. 
	 * It is the peak point of a triangle and the mid point on a plateau.
	 */
	double m_representativeValue;
public:
	FuzzySet(double maxVal):m_DOM(0), m_representativeValue(maxVal) {}

	/**
	 * Returns the degree of membership given a value.
	 */
	virtual double CalculateDOM(double value) const = 0;

	/** 
	 * This function OR's a value with the DOM.
	 * It sets the DOM to the maximum of parameter, or DOM.
	 * @param value Value to OR with DOM.
	 */
	void ORwithDOM(double value);

	double GetRepresentativeValue() const {return m_representativeValue;}
	void ClearDOM() {m_DOM = 0.0;}
	double GetDOM() const {return m_DOM;}
	void SetDOM(double value) {m_DOM = value;}
};

/**
 * A triangular fuzzy set.
 */
class FuzzySet_Triangle : public FuzzySet
{
public:
	FuzzySet_Triangle(double mid, double left, double right) : FuzzySet(mid), 
															   m_peakPoint(mid), 
															   m_leftOffset(left),
															   m_rightOffset(right)
	{
	#ifdef SAFE_VERSION
		if(IsEqual(m_rightOffset, 0.0) || IsEqual(m_leftOffset, 0.0) ||
			IsEqual(m_peakPoint, 0.0))
		{
			std::cout <<  "Invalid offsets in Triangular fuzzy set! Line: " << __LINE__ << "File: " << __FILE__ << std::endl;
		}
	#endif
	}

	/**
	 * Returns the degree of membership given a value.
	 */
	double CalculateDOM(double value) const;
private:
	double m_peakPoint;
	double m_leftOffset;
	double m_rightOffset;
};

/**
 * A right shoulder fuzzy set.
 */
class FuzzySet_RightShoulder : public FuzzySet
{
public:
	FuzzySet_RightShoulder(double peak, double leftOffset, double rightOffset) : FuzzySet(((peak + rightOffset) + peak)/2.0),
																				 m_peakPoint(peak),
																				 m_leftOffset(leftOffset) ,
																				 m_rightOffset(rightOffset){}
	/**
	 * Returns the degree of membership given a value.
	 */																			 
	double CalculateDOM(double value) const;
private:
	double m_peakPoint;
	double m_leftOffset;
	double m_rightOffset;
};

class FuzzySet_LeftShoulder : public FuzzySet
{
public:
	FuzzySet_LeftShoulder(double peak, double leftOffset, double rightOffset) : FuzzySet(((peak - leftOffset) + peak)/2.0),
																				 m_peakPoint(peak),
																				 m_leftOffset(leftOffset) ,
																				 m_rightOffset(rightOffset){}
	/**
	 * Returns the degree of membership given a value.
	 */																			 
	double CalculateDOM(double value) const;
private:
	double m_peakPoint;
	double m_leftOffset;
	double m_rightOffset;
};

#endif