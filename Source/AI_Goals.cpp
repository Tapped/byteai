#include "AI_Goals.h"
#include "SharedDefs.h"

void AtomicGoal::AddGoal(BaseGoal *goal)
{
    //This function is not implemented!
    Assert(false && "AtomicGoal::AddGoal is not implemented!");
}

CompositeGoal::~CompositeGoal()
{
    Clear();
}

void CompositeGoal::AddGoal(BaseGoal *goal)
{
    m_stackOfGoals.push(goal);
}

bool CompositeGoal::HandleMessage(Message &msg)
{
    //First send the message down to current atomic goal.
    if(!m_stackOfGoals.empty())
    {
        return m_stackOfGoals.top()->HandleMessage(msg);
    }

    return false;
}

int CompositeGoal::ProcessSubgoals()
{
    //Delete failed or completed subgoals.
    while(!m_stackOfGoals.empty() && 
        (m_stackOfGoals.top()->IsComplete() || m_stackOfGoals.top()->HasFailed()))
    {
        m_stackOfGoals.top()->End();
        SAFE_DELETE(m_stackOfGoals.top());
        m_stackOfGoals.pop();
    }

    if(!m_stackOfGoals.empty())
    {
        int status = m_stackOfGoals.top()->Process();
        Assert(m_stackOfGoals.top()->GetStatus() == status && "Remember to return the same status, that the status member!");
        
        if(status == GOAL_COMPLETE && m_stackOfGoals.size() > 1)
        {
            //Return that we have not completed all goals.
            return GOAL_ACTIVE;
        }
        
        return status;
    }
    else
    {
        return GOAL_COMPLETE;
    }
}

void CompositeGoal::Clear()
{
    while(!m_stackOfGoals.empty())
    {
        SAFE_DELETE(m_stackOfGoals.top());
        m_stackOfGoals.pop();
    }
}