#ifndef BYTEAI_AI_BRAIN_H_
#define BYTEAI_AI_BRAIN_H_

#include "AI_Goals.h"
#include "ThinkGoal.h"

/**
 * AI_Brain decides what the AI should do.
 */
class AI_Brain
{
public:
    /**
     * Init brain.
     */
    void Init();

    /**
     * Update brain.
     */
    void Update();
private:
    ThinkGoal m_goalBrain;//!< Manages the goal decisions.
};

#endif