#include <list>
#include "AttackDroidGoal.h"
#include "GameManager.h"
#include "Map.h"
#include "MessageHandler.h"

const TilePos kTilePosPossibilities[6] =
{
	TilePos(-1, -1), TilePos(1, 1), TilePos(0, -1),
	TilePos(0, 1), TilePos(-1, 0), TilePos(1, 0)
};

rapidjson::Document AttackDroidGoal::m_jsonDoc;
void AttackDroidGoal::Activate()
{
	m_status = GOAL_ACTIVE;
}

int AttackDroidGoal::Process()
{
	ActivateIfInactive();
    PrecacheJSONDocument();
 
	const Map *map = gGameMgr.GetMap();
	std::list<int> path;
	map->FindShortestPath(m_attacker->GetPosition(), m_target->GetPosition(), path);

	m_jsonDoc["sequence"].Clear();
	std::list<int>::iterator i;
	std::list<int>::iterator j;
	std::list<int>::iterator end = --path.end();

#ifdef DEBUG_ATTACK_DROID
	std::cout << "PATH BEGIN:" << std::endl;
#endif
	for(i=path.begin(); i != end; ++i) 
	{
		j = i;
		++j;

		const TilePos &fromTile = gGameMgr.GetMap()->GetNode(*i).GetPosition();
		const TilePos &toTile = gGameMgr.GetMap()->GetNode(*j).GetPosition();
	
		rapidjson::Value directionVal;
		directionVal.SetString(GetDirection(fromTile, toTile));
		
		m_jsonDoc["sequence"].PushBack(directionVal, m_jsonDoc.GetAllocator());

#ifdef DEBUG_ATTACK_DROID
		std::cout << GetDirection(fromTile, toTile) << " ";
		std::cout << std::endl;
#endif
	}
#ifdef DEBUG_ATTACK_DROID
	std::cout << "PATH END" << std::endl;
#endif

	gMessageHandler.Send(INVALID_RECIPIENT, RECIPIENT_NET, NET_SEND, 0, &m_jsonDoc);
		
	gGameMgr.SetOurTurn(false);

	m_status = GOAL_COMPLETE;

	return m_status;
}

void AttackDroidGoal::End()
{
}

void AttackDroidGoal::PrecacheJSONDocument()
{
	if(!m_jsonDoc.IsObject())
    {
		rapidjson::Value message;
        rapidjson::Value type;
        rapidjson::Value sequence;
 
		sequence.SetArray();
        message.SetString("action");
        type.SetString("droid");
               
        m_jsonDoc.SetObject();
        m_jsonDoc.AddMember("message", message, m_jsonDoc.GetAllocator());
        m_jsonDoc.AddMember("type", type, m_jsonDoc.GetAllocator());
        m_jsonDoc.AddMember("sequence", sequence, m_jsonDoc.GetAllocator());
    }
}