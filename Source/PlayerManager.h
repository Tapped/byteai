#ifndef BYTEAI_PLAYER_MANAGER_H_
#define BYTEAI_PLAYER_MANAGER_H_

#include "Recipient.h"
#include "Player.h"
#include "SharedDefs.h"
#include <map>

class PlayerManager : public Recipient
{
public:
    PlayerManager() : Recipient(RECIPIENT_PLAYERMGR), m_numPlayers(0), m_pOurPlayer(0) {}

    ~PlayerManager();

    /** 
     * Initialize player manager.
     */
    void Init();

    /**
     * Returns a pointer to the AI's player instance.
     */
    Player *GetOurPlayer();

    bool HasPlayer() const
    {
        return m_pOurPlayer != 0;
    }

	/**
	 * The number of players.
	 */
	int NumPlayers() const
	{
		return m_numPlayers;
	}

	const Player* FindClosestEnemy(const Player *owner) const;
    /**
     * Handle messages.
     */
    bool HandleMessage(Message &msg);

	class ConstPlayerIter
	{
	public:
        ConstPlayerIter(const PlayerManager &playerMgr) : m_playerMgr(playerMgr)
        {
			m_currentPlayer = m_playerMgr.m_players.begin();
        }

        const Player *Begin()
        {
            m_currentPlayer = m_playerMgr.m_players.begin();
			return &m_currentPlayer->second;
        }

        const Player *Next()
        {
            ++m_currentPlayer;
            if(End())
            {
                return 0;
            }

            return &m_currentPlayer->second;
        }

        bool End()
        {
			return m_currentPlayer == m_playerMgr.m_players.end();
        }
    private:
		const PlayerManager &m_playerMgr;
        std::map<std::string, Player>::const_iterator m_currentPlayer;
	};

	friend ConstPlayerIter;
private:
    void UpdatePlayers(Message &msg);

    std::map<std::string, Player> m_players;//!< Stores the players, where the name of the player is the key.
    Player *m_pOurPlayer;//!< Pointer to this AI's player instance.
    int m_numPlayers;//!< The number of players.
};

#endif