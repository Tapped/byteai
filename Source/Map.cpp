#include "Map.h"
#include "MessageHandler.h"
#include "GameManager.h"
#include <cassert>
#include <iostream>
#include <rapidjson/document.h>

void Map::Init()
{
	gMessageHandler.AddRecipient(this);
}

bool Map::HandleMessage(Message &msg)
{
	switch (msg.Msg)
	{
	case MAP_INIT:
		InitMap(msg);
        return true;
		break;
	case MAP_UPDATE:
		UpdateMap(msg);
        return true;
		break;
	default:
#ifdef _DEBUG
		std::cout << "Got unsupported message! Line: " << __LINE__ << "File: " << __FILE__ << std::endl;
#endif
		break;
	}

    return false;
}

void Map::InitMap(Message &msg)
{
	Assert(msg.ExtraData && "Map::InitMap() expected extra data in message.");
	ExtraData_Map *data = (ExtraData_Map*)msg.ExtraData; 

	m_jLength = data->jLength;
	m_kLength = data->kLength;

	//Fill map
	m_map = new char*[m_jLength];
	for(int j = 0;j < m_jLength;j++)
	{
		m_map[j] = new char[m_kLength];
		for(int k = 0;k < m_kLength;k++)
		{
            int type = data->Data[(j*m_kLength)+k];
			m_map[j][k] = type;
            ++m_nOfATileType[type];
            
            //Add node.
            int nodeIndex = m_navGraph.GetNextNodeIndex();
            m_navGraph.AddNode(NavGraphNode(nodeIndex, j, k));
        }
	}

    //Add edges to the graph.
    AddEdges();
}

void Map::AddEdges()
{
    //Add the edges to the graph.
    //We only add the edges that is accessible.
    for(int j = 0;j < m_jLength;j++)
    {
        for(int k = 0;k < m_kLength;k++)
        {
            int nodeIndex = (j * m_kLength) + k;
            if(!IsCompletelyInaccesibleTileType(GetType(j, k)))
            {
                if(k + 1 < m_kLength)
                {
                    if(!IsCompletelyInaccesibleTileType(GetType(j, k+1)))
                    {
                        GraphEdge edge(nodeIndex, nodeIndex + 1);
                        m_navGraph.AddEdge(edge);

                        if(j + 1 < m_jLength)
                        {
                            if(!IsCompletelyInaccesibleTileType(GetType(j+1, k+1)))
                            {
                                GraphEdge edge(nodeIndex, ((j+1)*m_kLength)+k+1);
                                m_navGraph.AddEdge(edge);
                            }
                        }
                    }
                }

                if(j + 1 < m_jLength)
                {
                    if(!IsCompletelyInaccesibleTileType(GetType(j+1, k)))
                    {
                        GraphEdge edge(nodeIndex, ((j+1)*m_kLength)+k);
                        m_navGraph.AddEdge(edge);
                    }
                }
            }
        }
    }
}

int Map::CostToTraverseTile(int j, int k) const
{
    int cost = 1;

    if(m_map[j][k] == TILE_ROCK || m_map[j][k] == TILE_VOID || m_map[j][k] == TILE_SPAWN)
    {
        cost = kMaxInt;
    }

    return cost;
}

void Map::UpdateMap(Message &msg)
{
	Assert(msg.ExtraData && "Map::UpdateMap() expected extra data in message.");
	ExtraData_Map *data = (ExtraData_Map*)msg.ExtraData; 

	m_jLength = data->jLength;
	m_kLength = data->kLength;

	for(int j = 0;j < m_jLength;j++)
	{
		for(int k = 0;k < m_kLength;k++)
		{
            int type = data->Data[(j*m_kLength)+k];
            int oldType = m_map[j][k];
			m_map[j][k] = type;
            if(oldType != type)
            {
                --m_nOfATileType[oldType];
                ++m_nOfATileType[type];
            }
		}
	}
}

int Map::AmountOfATileType(int type) const
{
    if(m_nOfATileType.find(type) != m_nOfATileType.end())
    {
        return m_nOfATileType.at(type);
    }
    else
    {
        return 0;
    }
}

int Map::MinDistanceToATileType(const TilePos &pos, int type) const
{
    int minDist = kMaxInt;
    for(int j = 0;j < m_jLength;j++)
    {
        for(int k = 0;k < m_kLength;k++)
        {
            TilePos tilePos(j, k);
            int dist;
            if(m_map[j][k] == type && (dist = Distance(tilePos, pos)) < minDist)
            {
                minDist = dist;
            }
        }
    }

    return minDist;
}

TilePos Map::FindClosestInstance(const TilePos &pos, int type) const
{
    TilePos bestTile;
	int minDist = kMaxInt;
    for(int j = 0;j < m_jLength;j++)
    {
        for(int k = 0;k < m_kLength;k++)
        {
            TilePos tilePos(j, k);
            int dist;
            if(m_map[j][k] == type && (dist = Distance(tilePos, pos)) < minDist)
            {
                bestTile = tilePos;
				minDist = dist;
            }
        }
    }
	
	return bestTile;
}

int Map::GetNodeIndex(const TilePos &tilePos) const
{
	if(tilePos.j < 0 || tilePos.k < 0)
	{
		return INVALID_NODE_INDEX;
	}

    int nodeIdx = tilePos.j * m_kLength + tilePos.k;
    if(nodeIdx >= m_navGraph.NumNodes() || nodeIdx < 0)
    {
        return INVALID_NODE_INDEX;
    }

    return nodeIdx;
}

const NavGraphNode &Map::GetNode(int nodeIdx) const
{
    return m_navGraph.GetNode(nodeIdx);
}

int Map::GetType(const TilePos &tilePos) const
{
    return m_map[tilePos.j][tilePos.k];
}

int Map::GetType(int j, int k) const
{
    return m_map[j][k];
}

bool Map::IsInaccesibleNode(int nodeIdx) const
{
	if(nodeIdx == INVALID_NODE_INDEX)
	{
		return true;
	}

	const TilePos &nodePos = GetNode(nodeIdx).GetPosition();
	PlayerManager::ConstPlayerIter playerIter(*gGameMgr.GetPlayerMgr());
	for(const Player *player = playerIter.Begin();
		!playerIter.End();player = playerIter.Next())
	{
		if(player != gGameMgr.GetPlayer())
		{
			if(nodePos == player->GetPosition())
			{
				return true;
			}
		}
	}

	return IsInaccesibleTileType(m_map[nodePos.j][nodePos.k]);
}

bool Map::IsInaccesibleTileType(int type) const
{
	return type == TILE_ROCK || type == TILE_VOID || type == TILE_SPAWN;
}

bool Map::IsCompletelyInaccesibleTileType(int type) const
{
	return type == TILE_ROCK || type == TILE_VOID;
}

void Map::FindShortestPath(const TilePos &source, const TilePos &target, std::list<int> &path) const
{
    AStarSearch search(m_navGraph, GetNodeIndex(source), GetNodeIndex(target));
    search.GetPath(path);
}

int Map::GetSize() const
{
    return m_jLength;
}

void Map::Release()
{
	if(m_map)
	{
		for(int j = 0;j < m_jLength;j++)
		{
			delete []m_map[j];
		}

		delete []m_map;
	}
}