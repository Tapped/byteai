#ifndef BYTEAI_SHARED_DEFS_H_
#define BYTEAI_SHARED_DEFS_H_

#include <cassert>
#include "MathUtil.h"

#ifdef _DEBUG
//#define DEBUG_NETWORK
#endif

#define DEFAULT_PORT "54321"
#define NETWORK_BUFFER_SIZE 16384

#define AI_NAME "Byte"
#define MAX_MAP_SIZE kMaxDouble
#define MAX_PLAYERS 8
#define MAX_GRAPH_PATH 1024
#define INVALID_NODE_INDEX -1

//Number of samples across a fuzzy manifold
#define FZ_NUM_SAMPLES_FOR_CENTROID 15

#ifdef SAFE_VERSION
	#ifdef NDEBUG
		#define Assert(x) (if(!x){return;})
	#else
		#define Assert(x) (assert(x))
	#endif
#else
	#define Assert(x)
#endif

#define SAFE_DELETE(x) if(x){delete x;x=0;}
#define SAFE_DELETE_ARRAY(x) if(x){delete[]x;x=0;}

#ifdef _MSC_VER
#define __STR2__(x) #x
#define __STR1__(x) __STR2__(x)
#define __LOC__ __FILE__ "("__STR1__(__LINE__)") : Warning Msg: "
#define CompilerWarning(x) message(__LOC__ x)
#else
#define CompilerWarning(x)
#endif

#endif