#ifndef BYTEAI_THINK_GOAL_H_
#define BYTEAI_THINK_GOAL_H_

#include "AI_Goals.h"
#include "Player.h"
#include "EvaluateAttack.h"
#include "EvaluateMine.h"
/**
 * Main goal, which evaluates goals, and choose the the best one.
 */
class ThinkGoal : public CompositeGoal
{
public:
    ThinkGoal() : CompositeGoal(RECIPIENT_THINKGOAL) {}

    /**
     * Function called before a goal is processed.
     */
    void Activate();

    /**
     * Function used to process an goal.
     */
    int Process();

    /**
     * Function used to clean up an goal.
     */
    void End();

    /**
     * Handle local messages.
     */
    bool HandleMessage(Message &msg);
private:
		EvaluateAttack evaluateAttack;
		EvaluateMine evaluateMine;
};

#endif