#ifndef BYTEAI_FOLLOW_TARGET_H_
#define BYTEAI_FOLLOW_TARGET_H_

#include "AI_Goals.h"
#include "Player.h"

/**
 * Follows a target, by updating a path to the target.
 */
class FollowTargetGoal : public CompositeGoal
{
public:
	FollowTargetGoal(const Player *target, const Player *attacker) : m_target(target), m_attacker(attacker) {}

	void Activate();

	int Process();

	void End();
private:
	const Player *m_target;
	const Player *m_attacker;
	TilePos m_oldTargetPos;//!< Stores the target position when the the activity function was called.
};

#endif