#ifndef BYTEAI_PLAYER_H_
#define BYTEAI_PLAYER_H_

#include <string>
#include "Weapons.h"
#include "Messages.h"
#include "TilePos.h"

/**
 * Player stores player specific information as weapons, health and score.
 */
class Player
{
public:
    Player();

    ~Player();

    /**
     * Set the player data for this player.
     * @param playerData Pointer to where the player data is.
     */
    void SetPlayerData(const PlayerData *playerData);

    const PlayerData *GetPlayerData() const;

    /**
     * Choose a weapon based on the best desirability score.
     * It will send the weapons to the server.
     * @param mapSize The size of the map.
     * @param numRes The number of resources.
     * @param minDist The distance to the nearest resource.
     */
    void ChooseWeapons(int mapSize, int numScrap, int numExplosium, int numRubidium);

	/**
	 * Returns the weapon at the specific index. 
	 */
	const Weapon *GetWeapon(int weaponIdx) const;

    void SetPosition(const TilePos &pos);

    const TilePos &GetPosition() const;

	const TilePos &GetOldPosition() const;

	void IncrementNumResources(char type);

	void UpgradeWeapons();
private:
    void AddWeapon(int wpnType, int slot);

	/**
	 * Set the weapon for the player.
	 */
	void SetWeapons();

	std::map<char, int> m_numResources;//!< The number of resources.

    PlayerData m_playerData;//!< Player data from server.
    TilePos m_clientPosition;//!< Predicted position, it is updated by the server.
	TilePos m_spawnPos;//!< The spawn position of player.
    Weapon *m_pWeapons[2]; //The player can store two weapons in the inventory.
    bool m_bHaveGotInitialPos;//!< Have we got initial position?
};

#endif