#include "FuzzyVariable.h"
#include "SharedDefs.h"

FuzzyVariable::~FuzzyVariable()
{
	for(FuzzySets::iterator iter = m_fuzzySets.begin();
		iter != m_fuzzySets.end();++iter)
	{
		SAFE_DELETE(iter->second);
	}
}

FzSet FuzzyVariable::AddTriangleSet(int ID, double minBound, double peak, double maxBound)
{
	FuzzySet_Triangle *pSet = new FuzzySet_Triangle(peak, peak - minBound, maxBound - peak);
	m_fuzzySets.insert(std::pair<int, FuzzySet*>(ID, pSet));

	AdjustRange(minBound, maxBound);

	return FzSet(pSet);
}

FzSet FuzzyVariable::AddLefShoulderSet(int ID, double minBound, double peak, double maxBound)
{
	FuzzySet_LeftShoulder *pSet = new FuzzySet_LeftShoulder(peak, peak - minBound, maxBound - peak);
	m_fuzzySets.insert(std::pair<int, FuzzySet*>(ID, pSet));

	AdjustRange(minBound, maxBound);

	return FzSet(pSet);
}

FzSet FuzzyVariable::AddRightShoulderSet(int ID, double minBound, double peak, double maxBound)
{
	FuzzySet_RightShoulder *pSet = new FuzzySet_RightShoulder(peak, peak - minBound, maxBound - peak);
	m_fuzzySets.insert(std::pair<int, FuzzySet*>(ID, pSet));

	AdjustRange(minBound, maxBound);

	return FzSet(pSet);
}

void FuzzyVariable::Fuzzify(double value)
{
    Assert(value >= m_minRange && value <= m_maxRange && "FuzzyVariable::Fuzzify() value is not in range.");

	for(FuzzySets::iterator iter = m_fuzzySets.begin();
		iter != m_fuzzySets.end();++iter)
	{
        double dom = iter->second->CalculateDOM(value);
		iter->second->SetDOM(dom);
	}
}

void FuzzyVariable::AdjustRange(double min, double max)
{
	if(min < m_minRange)
	{
		m_minRange = min;
	}
	if(max > m_maxRange)
	{
		m_maxRange = max;
	}
}

double FuzzyVariable::DeFuzzifyMaxAverage() const
{
	double repConfSum = 0.0f;
	double confidentSum = 0.0f;

	for(FuzzySets::const_iterator iter = m_fuzzySets.begin();
		iter != m_fuzzySets.end();++iter)
	{
		repConfSum += iter->second->GetDOM() * iter->second->GetRepresentativeValue();
		confidentSum += iter->second->GetDOM();
	}

    if(IsEqual(confidentSum, 0.0))
        return 0.0;

	return repConfSum / confidentSum;
}

double FuzzyVariable::DeFuzzifyCentroid(int numSamples) const
{
	double factor = (m_maxRange - m_minRange) / numSamples;
	double denominator = 0.0;
	double numerator = 0.0;

	for(int i = 0;i < numSamples;i++)
	{
		double step = i * factor;

		for(FuzzySets::const_iterator iter = m_fuzzySets.begin();
			iter != m_fuzzySets.end();++iter)
		{
            double dom = 0.0;
            dom = min(iter->second->CalculateDOM(m_minRange + step), 
                iter->second->GetDOM());
            
            numerator += (m_minRange + step) * dom;
            denominator += dom;
		}
	}

    if(IsEqual(denominator, 0.0f))
        return 0.0;

	return numerator / denominator;
}