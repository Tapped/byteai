#ifndef BYTEAI_FUZZY_MODULE_H_
#define BYTEAI_FUZZY_MODULE_H_

#include <map>
#include <vector>
#include "FuzzyVariable.h"
#include "FuzzyRule.h"
#include <iostream>

/**
 * Fuzzy module manages fuzzification, fuzzy rules and defuzzification. 
 */
class FuzzyModule
{
private:
	typedef std::map<int, FuzzyVariable*> FzVarMap;
	typedef std::vector<FuzzyRule*> FzRuleVec;
	FzVarMap m_fzVariables;
	
	std::vector<FuzzyRule*> m_fzRules;

	inline void ClearConsequent();
public:
	enum DefuzzifyTypes{MAX_AV, CENTROID};

	~FuzzyModule();
	
	/**
	 * Create an empty fuzzy variable.
	 * @param ID The ID of the fuzzy variable.
	 */
	FuzzyVariable &CreateFLV(int ID);

	/**
	 * Adds a rule for the module.
	 */
	void AddRule(FuzzyTerm &antecedent, FuzzyTerm &consequence);

	/**
	 * Fuzzify an value with this function.
	 * @param ID The ID of the fuzzy variable.
	 * @param value The value to fuzzify.
	 */
	inline void Fuzzify(int ID, double value);

	/**
	 * Defuzzify a fuzzy variable, to a crisp value.
	 * @param ID The ID of the fuzzy variable to defuzzify.
	 * @param method Which type defuzzify technique.
	 */
	inline double DeFuzzify(int ID, DefuzzifyTypes method);
};

void FuzzyModule::Fuzzify(int ID, double value)
{
	Assert(m_fzVariables.find(ID) != m_fzVariables.end() && "FuzzyModule::Fuzzify() could not find variable!");
	m_fzVariables[ID]->Fuzzify(value);
}

double FuzzyModule::DeFuzzify(int ID, DefuzzifyTypes method)
{
	Assert(m_fzVariables.find(ID) != m_fzVariables.end() && "FuzzyModule::DeFuzzify() could not find variable!");
	
	ClearConsequent();

	for(FzRuleVec::iterator iter = m_fzRules.begin();
		iter != m_fzRules.end();++iter)
	{
		(*iter)->Calculate();
	}

	switch(method)
	{
	case CENTROID:
		return m_fzVariables[ID]->DeFuzzifyCentroid(FZ_NUM_SAMPLES_FOR_CENTROID);
		break;
	case MAX_AV:
		return m_fzVariables[ID]->DeFuzzifyMaxAverage();
		break;
	default:
		std::cout << "Unrecognized method in Defuzzify! Only Centroid and Average of Maxima supported." << std::endl; 
		break;
	}

	return 0.0;
}

void FuzzyModule::ClearConsequent()
{
	for(FzRuleVec::iterator iter = m_fzRules.begin();
		iter != m_fzRules.end();++iter)
	{
		(*iter)->ClearConsequence();
	}
}
#endif