#ifndef BYTEAI_EVALUATE_ATTACK_H_
#define BYTEAI_EVALUATE_ATTACK_H_

#include "Evaluators.h"
#include "AI_Goals.h"
#include "Player.h"
#include "AttackGoal.h"
/*
	This class evaluates different factors to check if the ai should attack a target or not
*/

class EvaluateAttack : public EvaluatorBase
{
public:
	double CalculateDesirability();
	AttackGoal *GetGoal();
private:
	const Player *m_target;
	const Player *m_attacker;
};

#endif