#ifndef BYTEAI_EVALUATE_MINE_H_
#define BYTEAI_EVALUATE_MINE_H_

#include "Evaluators.h"
#include "AI_Goals.h"
#include "Player.h"
#include "MineGoal.h"

/*
	This class evaluates different factors to check if the ai should start mining for resources
*/

class EvaluateMine : public EvaluatorBase
{
public:
	double CalculateDesirability();
	MineGoal *GetGoal();
private:
	const Player *m_attacker;
	int m_targetType;
};

#endif