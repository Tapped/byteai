#include "FollowTargetGoal.h"
#include "MoveToGoal.h"
#include <iostream>

void FollowTargetGoal::Activate()
{
	m_status = GOAL_ACTIVE;

	Clear();

	m_oldTargetPos = m_target->GetPosition();
	AddGoal(new MoveToGoal(m_attacker->GetPosition(), m_oldTargetPos));
}

int FollowTargetGoal::Process()
{
	ActivateIfInactive();

	m_status = ProcessSubgoals();

	int dist = Distance(m_attacker->GetPosition(), m_target->GetPosition());
	if(m_oldTargetPos != m_target->GetPosition() || dist <= 2)
	{
		m_status = GOAL_COMPLETE;
	}

	return m_status;
}

void FollowTargetGoal::End()
{
	Clear();
}