#ifndef BYTEAI_MAP_H_
#define BYTEAI_MAP_H_

#include "Recipient.h"
#include "SharedDefs.h"
#include "MathUtil.h"
#include "TilePos.h"
#include <map>
#include <list>

#include "AStarGraphSearch.h"
#include "SparseGraph.h"
#include "GraphEdges.h"
#include "GraphNodes.h"
#include "HeuristicFunctions.h"

enum TILE_TYPES
{
	TILE_GRASS = 'G',
	TILE_VOID = 'V',
	TILE_SPAWN = 'S',
	TILE_EXPLOSIUM = 'E',
	TILE_RUBIDIUM = 'R',
	TILE_SCRAP = 'C',
	TILE_ROCK = 'O'
};

/**
 * Stores current map.
 */
class Map : Recipient
{
public:
    Map() : Recipient(RECIPIENT_MAP), m_map(0), m_navGraph(false) {}

	/**
	 * Initialize the map.
	 */
	void Init();

	/**
	 * Handle messages
	 */
	bool HandleMessage(Message &msg);

	/**
	 * Release any memory.
	 */
	void Release();

    /**
     * Returns the size of the map.
     */
    int GetSize() const;

    /**
     * The amount of tiles with the specific type.
     */
    int AmountOfATileType(int type) const;

    /**
     * Find the minimum distance to a specific tile type.
     */
    int MinDistanceToATileType(const TilePos &pos, int type) const;
	/**
	* Find the coordinates to the closest intance of a tile type
	*/
	TilePos FindClosestInstance(const TilePos &pos, int type) const;
    /**
     * Find the shortest path to a target position. It uses the A* algorithm.
     * @param target Target position.
     * @param path Where to output path.
     */
    void FindShortestPath(const TilePos &source, const TilePos &target, std::list<int> &path) const;

    /**
     * Returns node index of a tile, by using the position.
     */
    int GetNodeIndex(const TilePos &tilePos) const;

    /**
     * Returns a constant reference to a node.
     */
    const NavGraphNode &GetNode(int nodeIdx) const;

    /**
     * Returns the tile type at the specific tile position.
     */
    int GetType(const TilePos &tilePos) const;

    /**
     * Returns the tile type at the specific tile position.
     */
    int GetType(int j, int k) const;

    /**
     * Returns true if the tile type is inaccessible.
     */
    bool IsInaccesibleTileType(int type) const;

	bool IsInaccesibleNode(int nodeIdx) const;

    typedef SparseGraph<NavGraphNode, GraphEdge> NavGraph;
    typedef AStarGraphSearch<NavGraph, HeuristicManhattan> AStarSearch;
private:
	
	/**
	* Returns true if the tiletype is completely inaccessible.
	*/
	bool IsCompletelyInaccesibleTileType(int type) const;

	/**
	 * Internal functions which setups the map.
	 */
	void InitMap(Message &msg);

	/**
	 * Update the map
	 */
	void UpdateMap(Message &msg);

    /**
     * Add edges to the graph.
     */
    void AddEdges();

    /**
     * Returns the cost to traverse through or to this tile.
     */
    int CostToTraverseTile(int j, int k) const;

    NavGraph m_navGraph;//!< The navigation graph.

    std::map<int, int> m_nOfATileType;//!< Stores the number of tiles of a specific tile type.

	int m_jLength;//!< Map extent in j direction.
	int m_kLength;//!< Map extent in k direction.

	char **m_map;//!< Multi-dimensional array storing map information.
};

#endif