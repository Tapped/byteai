#include "MineGoal.h"
#include "GameManager.h"
#include "Map.h"
#include "MoveToGoal.h"
#include "MessageHandler.h"


rapidjson::Document MineGoal::m_jsonDoc;

void MineGoal::Activate()
{
	m_status = GOAL_ACTIVE;
	Clear();
	PrecacheJSONDocument();

	const Map *map = gGameMgr.GetMap();
	if(map->GetType(m_attacker->GetPosition()) != m_targetType)
	{
		AddGoal(new MoveToGoal(m_attacker->GetPosition(), map->FindClosestInstance(m_attacker->GetPosition(), m_targetType)));
	}
	else
	{
		gMessageHandler.Send(INVALID_RECIPIENT, RECIPIENT_NET, NET_SEND, 0, &m_jsonDoc);
		
		//Bad hack...
		Player *nonConstAttack = const_cast<Player*>(m_attacker);
		nonConstAttack->IncrementNumResources(m_targetType);
		gGameMgr.IncrementTurns();
		m_status = GOAL_COMPLETE;
	}
	
}

int MineGoal::Process()
{
	ActivateIfInactive();

	m_status = ProcessSubgoals();

	return m_status;
}

void MineGoal::End()
{

}

bool MineGoal::HandleMessage(Message &msg)
{
	return false;
}

void MineGoal::PrecacheJSONDocument()
{
    if(!m_jsonDoc.IsObject())
    {
        rapidjson::Value message;
        rapidjson::Value type;
 
        type.SetString("mine");
        message.SetString("action");              

		m_jsonDoc.SetObject();

        m_jsonDoc.AddMember("message", message, m_jsonDoc.GetAllocator());
        m_jsonDoc.AddMember("type", type, m_jsonDoc.GetAllocator());
    }
}