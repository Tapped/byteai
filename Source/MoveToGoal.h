#ifndef BYTEAI_MOVE_TO_GOAL_H_
#define BYTEAI_MOVE_TO_GOAL_H_

#include "AI_Goals.h"
#include "Map.h"
#include <list>

/**
 * Goal, that move the AI to a specific position.
 */
class MoveToGoal : public CompositeGoal
{
public:
    MoveToGoal() {}

    MoveToGoal(const TilePos &source, const TilePos &target) : m_sourcePosition(source), m_targetPosition(target) {}

    /**
     * Function called before an goal is processed.
     */
    void Activate();

    /**
     * Function used to process an goal.
     */
    int Process();

    /**
     * Function used to clean up an goal.
     */
    void End();

    /**
     * Handle local messages.
     */
    bool HandleMessage(Message &msg);
    
    /**
     * Set where to move from.
     */
    void SetSourcePos(const TilePos &sourcePos);

    /**
     * Set where to move.
     * @param pos Position to target position.
     */
    void SetTargetPos(const TilePos &targetPos);
private:
    std::list<int> m_path;//!< Path to follow.
    int m_oldNode;//!< Stores the old note from last iteration.
    TilePos m_sourcePosition;//!< The source position.
    TilePos m_targetPosition;//!< The target position.
};


#endif