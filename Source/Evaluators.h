#ifndef BYTEAI_EVALUATORS_H_
#define BYTEAI_EVALUATORS_H_

#include "AI_Goals.h"

class EvaluatorBase
{
public:
	virtual double CalculateDesirability() = 0;

	virtual BaseGoal *GetGoal() = 0;
protected:
	BaseGoal *m_goal;
};
#endif