#include "Weapons.h"
#include "SharedDefs.h"
#include "FuzzyModule.h"
#include "WeaponInfo.h"

enum FZVARS
{
    FZVAR_MAP_SIZE,
    FZVAR_RESOURCES,
    FZVAR_DESIRE,
};

enum FZSETS
{
    FZSET_SMALL_MAP,
    FZSET_MEDIUM_MAP,
    FZSET_LARGE_MAP,
    
    FZSET_FEW_RES,
    FZSET_ALOT_RES,
    FZSET_VERY_RES,

    FZSET_LOW_DESIRE,
    FZSET_MEDIUM_DESIRE,
    FZSET_LARGE_DESIRE
};

double WeaponDroid::CalculateDesirabilityAtStart(int mapSize, int numRes)
{
    FuzzyModule module;
    FuzzyVariable &fzMapSize = module.CreateFLV(FZVAR_MAP_SIZE);
    FuzzyVariable &fzResources = module.CreateFLV(FZVAR_RESOURCES);
    FuzzyVariable &fzDesire = module.CreateFLV(FZVAR_DESIRE);

    //These values needs to be tweaked.
    FzSet fzSmallMap = fzMapSize.AddLefShoulderSet(FZSET_SMALL_MAP, 0, 10, 16);
    FzSet fzMediumMap = fzMapSize.AddTriangleSet(FZSET_MEDIUM_MAP, 13, 20, 30);
    FzSet fzLargeMap = fzMapSize.AddRightShoulderSet(FZSET_LARGE_MAP, 20, 30, MAX_MAP_SIZE);

    FzSet fzFewRes = fzResources.AddLefShoulderSet(FZSET_FEW_RES, 0, 4, 6);
    FzSet fzAlotRes = fzResources.AddTriangleSet(FZSET_ALOT_RES, 4, 8, 10);
    FzSet fzVeryRes = fzResources.AddRightShoulderSet(FZSET_VERY_RES, 8, 10, MAX_MAP_SIZE);

    FzSet fzLowDesire = fzDesire.AddLefShoulderSet(FZSET_LOW_DESIRE, 0, 40, 50);
    FzSet fzMediumDesire = fzDesire.AddTriangleSet(FZSET_MEDIUM_DESIRE, 40, 50, 80);
    FzSet fzHighDesire = fzDesire.AddRightShoulderSet(FZSET_LARGE_DESIRE, 60, 80, 100);

    module.AddRule(FzAnd(fzFewRes, fzSmallMap), fzLowDesire);
    module.AddRule(FzAnd(fzFewRes, fzMediumMap), fzMediumDesire);
    module.AddRule(FzAnd(fzFewRes, fzLargeMap), fzLowDesire);

    module.AddRule(FzAnd(fzAlotRes, fzSmallMap), fzLowDesire);
    module.AddRule(FzAnd(fzAlotRes, fzMediumMap), fzHighDesire);
    module.AddRule(FzAnd(fzAlotRes, fzLargeMap), fzHighDesire);

    module.AddRule(FzAnd(fzVeryRes, fzSmallMap), fzLowDesire);
    module.AddRule(FzAnd(fzVeryRes, fzMediumMap), fzHighDesire);
    module.AddRule(FzAnd(fzVeryRes, fzLargeMap), fzHighDesire);

    module.Fuzzify(FZVAR_MAP_SIZE, mapSize);
    module.Fuzzify(FZVAR_RESOURCES, numRes);

    return module.DeFuzzify(FZVAR_DESIRE, FuzzyModule::CENTROID);
}

double WeaponMortar::CalculateDesirabilityAtStart(int mapSize, int numRes)
{
    FuzzyModule module;
    FuzzyVariable &fzMapSize = module.CreateFLV(FZVAR_MAP_SIZE);
    FuzzyVariable &fzResources = module.CreateFLV(FZVAR_RESOURCES);
    FuzzyVariable &fzDesire = module.CreateFLV(FZVAR_DESIRE);

    //These values needs to be tweaked.
    FzSet fzSmallMap = fzMapSize.AddLefShoulderSet(FZSET_SMALL_MAP, 0, 10, 16);
    FzSet fzMediumMap = fzMapSize.AddTriangleSet(FZSET_MEDIUM_MAP, 13, 20, 30);
    FzSet fzLargeMap = fzMapSize.AddRightShoulderSet(FZSET_LARGE_MAP, 20, 30, MAX_MAP_SIZE);

    FzSet fzFewRes = fzResources.AddLefShoulderSet(FZSET_FEW_RES, 0, 4, 6);
    FzSet fzAlotRes = fzResources.AddTriangleSet(FZSET_ALOT_RES, 4, 8, 10);
    FzSet fzVeryRes = fzResources.AddRightShoulderSet(FZSET_VERY_RES, 8, 10, MAX_MAP_SIZE);

    FzSet fzLowDesire = fzDesire.AddLefShoulderSet(FZSET_LOW_DESIRE, 0, 40, 50);
    FzSet fzMediumDesire = fzDesire.AddTriangleSet(FZSET_MEDIUM_DESIRE, 40, 50, 80);
    FzSet fzHighDesire = fzDesire.AddRightShoulderSet(FZSET_LARGE_DESIRE, 60, 80, 100);

    module.AddRule(FzAnd(fzFewRes, fzSmallMap), fzMediumDesire);
    module.AddRule(FzAnd(fzFewRes, fzMediumMap), fzMediumDesire);
    module.AddRule(FzAnd(fzFewRes, fzLargeMap), fzLowDesire);

    module.AddRule(FzAnd(fzAlotRes, fzSmallMap), fzLowDesire);
    module.AddRule(FzAnd(fzAlotRes, fzMediumMap), fzHighDesire);
    module.AddRule(FzAnd(fzAlotRes, fzLargeMap), fzHighDesire);

    module.AddRule(FzAnd(fzVeryRes, fzSmallMap), fzLowDesire);
    module.AddRule(FzAnd(fzVeryRes, fzMediumMap), fzHighDesire);
    module.AddRule(FzAnd(fzVeryRes, fzLargeMap), fzHighDesire);

    module.Fuzzify(FZVAR_MAP_SIZE, mapSize);
    module.Fuzzify(FZVAR_RESOURCES, numRes);

    return module.DeFuzzify(FZVAR_DESIRE, FuzzyModule::CENTROID);
}

double WeaponLaser::CalculateDesirabilityAtStart(int mapSize, int numRes)
{
    FuzzyModule module;
    FuzzyVariable &fzMapSize = module.CreateFLV(FZVAR_MAP_SIZE);
    FuzzyVariable &fzResources = module.CreateFLV(FZVAR_RESOURCES);
    FuzzyVariable &fzDesire = module.CreateFLV(FZVAR_DESIRE);

    //These values needs to be tweaked.
    FzSet fzSmallMap = fzMapSize.AddLefShoulderSet(FZSET_SMALL_MAP, 0, 10, 16);
    FzSet fzMediumMap = fzMapSize.AddTriangleSet(FZSET_MEDIUM_MAP, 13, 20, 30);
    FzSet fzLargeMap = fzMapSize.AddRightShoulderSet(FZSET_LARGE_MAP, 20, 30, MAX_MAP_SIZE);

    FzSet fzFewRes = fzResources.AddLefShoulderSet(FZSET_FEW_RES, 0, 4, 6);
    FzSet fzAlotRes = fzResources.AddTriangleSet(FZSET_ALOT_RES, 4, 8, 10);
    FzSet fzVeryRes = fzResources.AddRightShoulderSet(FZSET_VERY_RES, 8, 10, MAX_MAP_SIZE);

    FzSet fzLowDesire = fzDesire.AddLefShoulderSet(FZSET_LOW_DESIRE, 0, 40, 50);
    FzSet fzMediumDesire = fzDesire.AddTriangleSet(FZSET_MEDIUM_DESIRE, 40, 50, 80);
    FzSet fzHighDesire = fzDesire.AddRightShoulderSet(FZSET_LARGE_DESIRE, 60, 80, 100);

    module.AddRule(FzAnd(fzFewRes, fzSmallMap), fzMediumDesire);
    module.AddRule(FzAnd(fzFewRes, fzMediumMap), fzMediumDesire);
    module.AddRule(FzAnd(fzFewRes, fzLargeMap), fzMediumDesire);

    module.AddRule(FzAnd(fzAlotRes, fzSmallMap), fzHighDesire);
    module.AddRule(FzAnd(fzAlotRes, fzMediumMap), fzHighDesire);
    module.AddRule(FzAnd(fzAlotRes, fzLargeMap), fzHighDesire);

    module.AddRule(FzAnd(fzVeryRes, fzSmallMap), fzHighDesire);
    module.AddRule(FzAnd(fzVeryRes, fzMediumMap), fzHighDesire);
    module.AddRule(FzAnd(fzVeryRes, fzLargeMap), fzHighDesire);

    module.Fuzzify(FZVAR_MAP_SIZE, mapSize);
    module.Fuzzify(FZVAR_RESOURCES, numRes);

    return module.DeFuzzify(FZVAR_DESIRE, FuzzyModule::CENTROID);
}

double WeaponLaser::CalculateDesirability(int distance) const
{
	double rangeFactor;
	if(distance == 0)
	{
		return 100.0;
	}
	else if(distance > m_range[m_level])
	{
		return 0.0;
	}
	else 
	{
		rangeFactor =  (double)m_range[m_level] / (double)distance;
		rangeFactor *= 10;
	}

	double weaponDamage = (double)m_damage[m_level] / (double)MUCH_DAMAGE;

	return rangeFactor * weaponDamage;
}

double WeaponMortar::CalculateDesirability(int distance) const
{
	double rangeFactor;
	
	//We don't want to hurt ourself...
	if(distance == 0)
	{
		return 0.0;
	}
	else if(distance > m_range[m_level])
	{
		return 0.0;
	}
	else 
	{
		rangeFactor = (double)m_range[m_level] / (double)distance;
		rangeFactor *= 100;
	}

	double weaponDamage =  (double)m_damage[m_level] / (double)MUCH_DAMAGE;

	return rangeFactor * weaponDamage;
}

double WeaponDroid::CalculateDesirability(int distance) const
{
	double rangeFactor;
	
	//We don't want to hurt ourself...
	if(distance == 0)
	{
		return 100.0;
	}
	else if(distance > m_range[m_level])
	{
		return 0.0;
	}
	else 
	{
		rangeFactor =  (double)m_range[m_level] / (double)distance;
		rangeFactor *= 100;
	}

	double weaponDamage =  (double)m_damage[m_level] / (double)MUCH_DAMAGE;

	return rangeFactor * weaponDamage;
}