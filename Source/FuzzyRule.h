#ifndef BYTEAI_FUZZY_RULE_H_
#define BYTEAI_FUZZY_RULE_H_

#include "FuzzyTerms.h"
#include "SharedDefs.h"

/**
 * Fuzzy rule contains two fuzzy terms. An antecedent and a consequence.
 */
class FuzzyRule
{
public:
	FuzzyRule(FuzzyTerm &ant, FuzzyTerm &con) : m_pAntecedent(ant.Clone()), m_pConsequence(con.Clone()) {}

	~FuzzyRule() {SAFE_DELETE(m_pAntecedent); SAFE_DELETE(m_pConsequence);}

	void ClearConsequence() {m_pConsequence->ClearDOM();}

	/**
	 * Update the DOM for consequence.
	 */
	void Calculate()
	{
		m_pConsequence->ORwithDOM(m_pAntecedent->GetDOM());
	}
private:
	FuzzyRule(const FuzzyRule &);
	FuzzyRule &operator=(const FuzzyRule&);

	const FuzzyTerm *m_pAntecedent;

	FuzzyTerm *m_pConsequence;
};

#endif