#ifndef BYTEAI_MESSAGES_H_
#define BYTEAI_MESSAGES_H_

#include "MathUtil.h"
#include "SharedDefs.h"
#include <vector>
#include "TilePos.h"

enum Messages
{
	INVALID_MESSAGE,
	MAP_INIT,
	MAP_UPDATE,
	NET_SEND,
    PLAYERS_UPDATE,
};

struct Message
{
	uint From;
	uint To;
	uint Msg;
	uint DispatchTime;
	void *ExtraData;

	Message()
	{
	}

	Message(uint from, uint to, uint message, void *extraData)
	{
		To = to;
		From = from;
		Msg = message;
		DispatchTime = 0;
		ExtraData = extraData;
	}

	bool operator <(const Message& msg) const
	{
		return DispatchTime < msg.DispatchTime;
	}
};

struct ExtraData_Map
{
	int jLength;
	int kLength;
	char *Data;
};

struct PlayerData
{
    std::string name;
    std::string primWeapon;
    std::string secondWeapon;
    int score;
    TilePos position;
    int levelPrimWeapon;
    int levelSecondWeapon;
    int health;
};

struct ExtraData_Players
{
    PlayerData players[MAX_PLAYERS];
    int numPlayers;
};

#endif